package edu.pnu.stem.indoor.util.parser;

import org.locationtech.jts.geom.Coordinate;

public class Trajectory {

    private Coordinate[] coordinates;
    private String[] labels;

    public Trajectory(Coordinate[] geometry, String[] labels) {
        coordinates = geometry.clone();
        this.labels = labels.clone();
    }

    public Coordinate[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinate[] coordinates) {
        this.coordinates = coordinates;
    }

    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }
}
