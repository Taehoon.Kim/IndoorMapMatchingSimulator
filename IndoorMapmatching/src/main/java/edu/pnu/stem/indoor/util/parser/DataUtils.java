package edu.pnu.stem.indoor.util.parser;

import cn.edu.zju.db.datagen.database.DB_Connection;
import cn.edu.zju.db.datagen.database.DB_WrapperLoad;
import cn.edu.zju.db.datagen.database.spatialobject.AccessPoint;
import cn.edu.zju.db.datagen.database.spatialobject.Floor;
import cn.edu.zju.db.datagen.database.spatialobject.Partition;
import diva.util.java2d.Polygon2D;
import edu.pnu.stem.indoor.feature.CellSpace;
import edu.pnu.stem.indoor.feature.IndoorFeatures;
import org.davidmoten.hilbert.HilbertCurve;
import org.davidmoten.hilbert.SmallHilbertCurve;
import org.locationtech.jts.geom.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class DataUtils {
    public static final int GRID_SIZE = 100;
    public static  SmallHilbertCurve hilbertCurve = HilbertCurve.small().bits(8).dimensions(2);
    private static final GeometryFactory gf = new GeometryFactory();
    private static int floorID = 0;

    public static IndoorFeatures getIndoorFeaturesFromOSMXML(File inputFile) throws ParserConfigurationException, IOException, SAXException {
        InputStream inputStream = new FileInputStream(inputFile);
        return getIndoorFeaturesFromOSMXML(inputStream);
    }

    public static IndoorFeatures getIndoorFeaturesFromOSMXML(InputStream inputStream) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document document = dBuilder.parse(inputStream);
        document.getDocumentElement().normalize();

        HashMap<Integer, Coordinate> osmNodeInfo = new HashMap<>();

        double max_position_x = 0;
        double max_position_y = 0;
        double min_position_x = 10000;
        double min_position_y = 10000;

        // Save node information in osm xml file (It is point geometry related with Cell geometries)
        NodeList nodeList = document.getElementsByTagName("node");
        for(int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                int id = Integer.parseInt(element.getAttribute("id"));
                double x = Double.parseDouble(element.getAttribute("lat"));
                double y = Double.parseDouble(element.getAttribute("lon"));
                osmNodeInfo.put(id, new Coordinate(x,y));

                if (x > max_position_x) max_position_x = x;
                if (y > max_position_y) max_position_y = y;
                if (x < min_position_x) min_position_x = x;
                if (y < min_position_y) min_position_y = y;
            }
        }

        ChangeCoord.setInitialInfo(new Coordinate(min_position_x, min_position_y), new Coordinate(max_position_x,max_position_y));
        ArrayList<CellSpace> cellSpaces = new ArrayList<>();
        ArrayList<LineString> doorGeoms = new ArrayList<>();
        ArrayList<Coordinate> coords = new ArrayList<>();

        // Make cell spaces (relate with geometry, door info, cell label)
        NodeList wayList = document.getElementsByTagName("way");
        for(int i = 0; i < wayList.getLength(); i++) {
            Node node = wayList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                NodeList refList = element.getElementsByTagName("nd");
                NodeList tagList = element.getElementsByTagName("tag");

                for(int j = 0; j < refList.getLength(); j++) {
                    Node refNode = refList.item(j);
                    if(refNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element refElement = (Element) refNode;
                        int refID = Integer.parseInt(refElement.getAttribute("ref"));
                        coords.add(ChangeCoord.changeCoordWGS84toMeter(osmNodeInfo.get(refID)));
                    }
                }
                Coordinate[] coordsArray = new Coordinate[coords.size()];
                for(int j = 0; j < coords.size(); j++) {
                    coordsArray[j] = coords.get(j);
                }

                CellSpace cellSpace = null;
                String cellLabel = null;
                for(int j = 0; j < tagList.getLength(); j++) {
                    Node tagNode = tagList.item(j);
                    if (tagNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element tagElement = (Element) tagNode;
                        if(tagElement.getAttribute("k").equals("door")
                                && tagElement.getAttribute("v").equals("yes")) {
                            LineString lineString = gf.createLineString(coordsArray);
                            doorGeoms.add(lineString);
                            break;
                        }
                        else if((tagElement.getAttribute("k").equals("room") || tagElement.getAttribute("k").equals("corridor"))
                                && tagElement.getAttribute("v").equals("yes")) {
                            if(coords.get(0).equals(coords.get(coords.size() - 1))) {
                                // In case : "way" geometry is closed polygon
                                CoordinateSequence seq = gf.getCoordinateSequenceFactory().create(coordsArray);
                                LinearRing lr = gf.createLinearRing(seq);
                                Polygon polygon = gf.createPolygon(lr);
                                cellSpace = new CellSpace(polygon);
                                cellSpaces.add(cellSpace);
                            }
                        }
                        if(tagElement.getAttribute("k").equals("label")) {
                            cellLabel = tagElement.getAttribute("v");
                        }
                    }
                }

                if(cellSpace != null && cellLabel != null){
                    cellSpace.setLabel(cellLabel);
                }

                coords.clear();
            }
        }

        IndoorFeatures indoorFeatures = new IndoorFeatures();
        for(CellSpace cellSpace : cellSpaces) {
            Polygon cellGeom = cellSpace.getGeom();
            for(LineString doorGeom : doorGeoms) {
                if(cellGeom.covers(doorGeom)) {
                    cellSpace.addDoors(doorGeom);
                }
            }
            indoorFeatures.addCellSpace(cellSpace);
        }

        return indoorFeatures;
    }

    public static LineString getBuildNGoData(InputStream inputStream) throws IOException {
        ArrayList<Coordinate> coords = new ArrayList<>();
        BufferedReader reader= new BufferedReader(new InputStreamReader(inputStream,"EUC_KR"));

        String line;
        while((line = reader.readLine()) != null) {
            if(line.split("/").length > 1) {
                Coordinate coord = new Coordinate(Double.parseDouble(line.split("/")[1]),
                        Double.parseDouble(line.split("/")[0]));
                coords.add(new Coordinate(ChangeCoord.changeCoordWGS84toMeter(coord)));
            }
        }

        if(coords.size() > 1) {
            Coordinate[] trajectoryData = new Coordinate[coords.size()];
            for(int i = 0; i < coords.size(); i++) {
                trajectoryData[i] = coords.get(i);
            }
            return gf.createLineString(trajectoryData);
        }
        else {
            return gf.createLineString(new Coordinate[]{});
        }
    }

    public static LineString getBuildNGoData(File inputFile) throws IOException {
        InputStream inputStream = new FileInputStream(inputFile);
        return getBuildNGoData(inputStream);
    }

    public static String[] getBuildNGoGroundTruth(File inputFile) throws IOException {
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        ArrayList<String> groundTruth = new ArrayList<>();

        while((line = reader.readLine()) != null) {
            groundTruth.add(line);
        }

        if(groundTruth.size() > 1) {
            String[] results = new String[groundTruth.size()];
            for(int i = 0; i < groundTruth.size(); i++) {
                results[i] = groundTruth.get(i);
            }
            return results;
        }
        else
            return null;
    }

    public static Trajectory[] getDeepLearningResults(File inputFile) throws IOException {
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        ArrayList<String> deepLearningResults = new ArrayList<>();

        while((line = reader.readLine()) != null) {
            deepLearningResults.add(line);
        }

        if(deepLearningResults.size() > 1) {
            Trajectory[] results = new Trajectory[deepLearningResults.size()];
            for(int i = 0; i < deepLearningResults.size(); i++) {
                String[] coords = deepLearningResults.get(i).split(" ");
                Coordinate[] coordinates = new Coordinate[coords.length - 1];
                String[] labels = new String[coordinates.length];
                for(int j = 0; j < coordinates.length; j++) {
                    String[] label = coords[j].split("@");
                    labels[j]      = label[0];
                    if(label.length != 1) {
                        //String[] coord = label[1].split("_");
                        //coordinates[j] = new Coordinate(Integer.valueOf(coord[0]) * DataUtils.GRID_SIZE, Integer.valueOf(coord[1]) * DataUtils.GRID_SIZE);

                        // Using SFC code for gird layout
                        long[] coord   = hilbertCurve.point(Integer.parseInt(label[1]));
                        coordinates[j] = new Coordinate(coord[0] * DataUtils.GRID_SIZE, coord[1] * DataUtils.GRID_SIZE);
                    }
                }
                results[i] = new Trajectory(coordinates, labels);
            }
            return results;
        }
        else
            return null;
    }

    public static LineString[] getTrajectory(File inputFile) {
        String line;
        BufferedReader reader;
        ArrayList<String> trajectories = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader(inputFile));
            while((line = reader.readLine()) != null) {
                trajectories.add(line);
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }

        //if(trajectories.size() > 1) {
            LineString[] results = new LineString[trajectories.size()];
            for(int i = 0; i < trajectories.size(); i++) {
                String[] coords = trajectories.get(i).split(" ");
                Coordinate[] coordinates = new Coordinate[coords.length];
                for(int j = 0; j < coords.length; j++) {
                    String[] coord = coords[j].split("_");
                    coordinates[j] = new Coordinate(Double.parseDouble(coord[0]), Double.parseDouble(coord[1]));
                }
                results[i] = gf.createLineString(coordinates);
            }
            return results;
        //}
        //else
        //    return null;
    }

    public static IndoorFeatures getIndoorInfoWithIFCFormat() {
        final int buildingID = 9;
        final int floorNumber = 24;

        IndoorFeatures indoorFeatures = new IndoorFeatures();
        String confPath = System.getProperty("user.dir") + File.separator + "IndoorMapmatching" + File.separator + "conf" + File.separator + "MooveWork.properties";
        Connection connection = DB_Connection.connectToDatabase(confPath);
        try {
            DB_WrapperLoad.loadALL(connection, buildingID);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<Floor> floors = DB_WrapperLoad.floorT;
        Floor floor = null;
        for(Floor tmpFloor: floors) {
            if(tmpFloor.getItemID() == floorNumber) {
                floor = tmpFloor;
                break;
            }
        }
        assert floor != null;
        floorID = floor.getItemID();
        //floorID = 3;

        double max_position_x = 0;
        double max_position_y = 0;
        double min_position_x = 10000;
        double min_position_y = 10000;
        for(Partition partition : floor.getPartsAfterDecomposed()) {
            if(partition.getPolygon2D().getVertexCount() == 0) // In Case: OutSide
                continue;
            Rectangle2D mbr = partition.getPolygon2D().getBounds2D();
            if (mbr.getMaxX() > max_position_x) max_position_x = mbr.getMaxX();
            if (mbr.getMaxY() > max_position_y) max_position_y = mbr.getMaxY();
            if (mbr.getMinX() < min_position_x) min_position_x = mbr.getMinX();
            if (mbr.getMinY() < min_position_y) min_position_y = mbr.getMinY();
        }
        ChangeCoord.setInitialInfo(new Coordinate(min_position_x, min_position_y), new Coordinate(max_position_x,max_position_y));

        for(Partition partition : floor.getPartitions()) {
            Polygon2D.Double cellGeom = partition.getPolygon2D();
            if(cellGeom.getVertexCount() == 0) // In Case: OutSide
                continue;
            Coordinate[] coordsArray = new Coordinate[cellGeom.getVertexCount() + 1];
            for(int i = 0; i < coordsArray.length - 1; i++) {
                coordsArray[i] = ChangeCoord.changeCoordWithRatio(new Coordinate(cellGeom.getX(i), cellGeom.getY(i)));
            }
            coordsArray[coordsArray.length - 1] = coordsArray[0];

            CoordinateSequence seq = gf.getCoordinateSequenceFactory().create(coordsArray);
            LinearRing lr = gf.createLinearRing(seq);
            Polygon polygon = gf.createPolygon(lr);
            CellSpace cellSpace = new CellSpace(polygon);
            if(partition.getName() != null)
                cellSpace.setLabel(partition.getName());

            for(AccessPoint ap : partition.getAPs()) {
                Line2D.Double doorGeom = ap.getLine2D();
                coordsArray = new Coordinate[2];
                coordsArray[0] = ChangeCoord.changeCoordWithRatio(new Coordinate(doorGeom.getX1(), doorGeom.getY1()));
                coordsArray[1] = ChangeCoord.changeCoordWithRatio(new Coordinate(doorGeom.getX2(), doorGeom.getY2()));
                LineString doorGeomJTS = gf.createLineString(coordsArray);
                if(cellSpace.getGeom().covers(doorGeomJTS)) {
                    cellSpace.addDoors(doorGeomJTS);
                } else {
                    double bufferSize = cellSpace.getGeom().distance(doorGeomJTS);

                    Geometry buffer = doorGeomJTS.buffer(bufferSize + 0.2);
                    LineString newDoorGeomJTS = (LineString) buffer.intersection(cellSpace.getGeom().getExteriorRing());//cellSpace.getGeom().intersection(buffer);
                    if(cellSpace.getGeom().covers(newDoorGeomJTS)) {
                        cellSpace.addDoors(newDoorGeomJTS);
                    } else
                        System.out.println("WTF??");
                }

            }
            indoorFeatures.addCellSpace(cellSpace);
        }

        System.out.println("# of space: " + indoorFeatures.getCellSpaces().size());
        return indoorFeatures;
    }

    public static LineString getVITAData(File inputFile) throws IOException, ParseException {
        String line;
        ArrayList<Coordinate> coords = new ArrayList<>();
        ArrayList<Coordinate> coordsInASec = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        Date prevDate = null;
        boolean isRawData = false;
        line = reader.readLine(); // first line is header part (ignore)

        while((line = reader.readLine()) != null) {
            if(line.split("\t").length > 1) {
                String[] parsedResult = line.split("\t");
                // parsedResult is composed as follows
                // floorId | partitionId | location_x | location_y | timeStamp
                if(floorID != Integer.parseInt(parsedResult[0])) {
                    break;
                }

                Coordinate coord = new Coordinate(Double.parseDouble(parsedResult[2]),
                        Double.parseDouble(parsedResult[3]));
                coordsInASec.add(coord);
                if(!isRawData) {
                    coords.add(new Coordinate(ChangeCoord.changeCoordWithRatio(coord)));
                }

                SimpleDateFormat dt = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                Date curDate = dt.parse(parsedResult[4]);

                if(prevDate == null) {
                    prevDate = curDate;
                }
                else if(!isRawData) {
                    if(prevDate.equals(curDate)) {
                        isRawData = true;
                        if(coords.size() == 3) {
                            Coordinate first = coords.get(0);
                            coords.clear();
                            coords.add(first);
                        }
                        else {
                            coords.clear();
                        }
                    }
                }
                else {
                    if(!prevDate.equals(curDate)) {
                        // create an average coordinate while in a second
                        long timeGap = (curDate.getTime() - prevDate.getTime()) / 1000;

                        Coordinate nextCoord = coordsInASec.get(coordsInASec.size() - 1);
                        coordsInASec.remove(coordsInASec.size() - 1);

                        Coordinate coordinate = getAverageCoordinate(coordsInASec);
                        for(int i = 0; i < timeGap; i++)
                            coords.add(new Coordinate(ChangeCoord.changeCoordWithRatio(coordinate)));
                        coordsInASec.clear();
                        coordsInASec.add(nextCoord);
                    }
                }

                prevDate = curDate;
            }
        }

        if(!coordsInASec.isEmpty() && isRawData) {
            Coordinate coordinate = getAverageCoordinate(coordsInASec);
            coords.add(new Coordinate(ChangeCoord.changeCoordWithRatio(coordinate)));
            coordsInASec.clear();
        }

        if(coords.size() > 10) {
            Coordinate[] trajectoryData = new Coordinate[coords.size()];
            for(int i = 0; i < coords.size(); i++) {
                trajectoryData[i] = coords.get(i);
            }
            return gf.createLineString(trajectoryData);
        }
        else
            return gf.createLineString(new Coordinate[]{});
    }

    private static Coordinate getAverageCoordinate(ArrayList<Coordinate> coordsInASec) {
        double avrCoordX = 0;
        double avrCoordY = 0;
        for(Coordinate c : coordsInASec) {
            avrCoordX += c.x;
            avrCoordY += c.y;
        }
        avrCoordX /= coordsInASec.size();
        avrCoordY /= coordsInASec.size();

        return new Coordinate(avrCoordX,avrCoordY);
    }
}
