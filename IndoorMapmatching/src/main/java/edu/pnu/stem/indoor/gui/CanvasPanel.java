package edu.pnu.stem.indoor.gui;

import edu.pnu.stem.indoor.feature.CellSpace;
import edu.pnu.stem.indoor.feature.IndoorFeatures;
import edu.pnu.stem.indoor.util.movingobject.synthetic.SyntheticDataElement;
import edu.pnu.stem.indoor.util.parser.ChangeCoord;
import edu.pnu.stem.indoor.util.IndoorUtils;
import edu.pnu.stem.indoor.util.movingobject.synthetic.SyntheticTrajectoryGenerator;
import edu.pnu.stem.indoor.util.movingobject.synthetic.TimeTableElement;
import edu.pnu.stem.indoor.util.mapmatching.DirectIndoorMapMatching;
import edu.pnu.stem.indoor.util.mapmatching.HMMIndoorMapMatching;
import edu.pnu.stem.indoor.util.parser.DataUtils;
import edu.pnu.stem.indoor.util.parser.Trajectory;

import info.debatty.java.stringsimilarity.Levenshtein;
import info.debatty.java.stringsimilarity.NGram;
import info.debatty.java.stringsimilarity.QGram;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.LineString;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by STEM_KTH on 2017-05-17.
 * @author Taehoon Kim, Pusan National University, STEM Lab.
 */
public class CanvasPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener, KeyListener {
    private static final double SNAP_THRESHOLD  = 10;   // Threshold value for snapping function
    private static final double POINT_RADIUS    = 5;    // Radius of point for draw point (circle shape)
    private static final double SCREEN_BUFFER   = 50;   // Screen buffer value
    private static final int    ARR_SIZE        = 8;    // Arrow size of directed line
    private static final double MAX_DISTANCE    = ChangeCoord.CANVAS_MULTIPLE * 5;  // The distance that humans can move per unit time
    private static final double MIN_DISTANCE    = ChangeCoord.CANVAS_MULTIPLE * 0.5;

    EditStatus currentEditStatus = null;

    private int selectedCellIndex   = -1;
    private final String resultPath = "result" + File.separator;

    private GeometryFactory gf;
    private IndoorFeatures indoorFeatures;
    private LineString trajectory = null;
    private LineString trajectory_IF = null;
    private LineString trajectory_GT = null;
    private Coordinate[] drawingCellCoords = null;
    private Coordinate trajectoryCoords = null;
    private ArrayList<LineString> relatedVisibilityEdge;
    private ArrayList<LineString> relatedD2DEdge;
    private ArrayList<Polygon> circleBufferArray;
    private IndoorMapmatchingSim parent;

    private int mousePositionX;
    private int mousePositionY;

    private int dataIndex = 0;
    private boolean rawTrajectoryVisibility;
    private boolean noiseTrajectoryVisibility;
    private boolean groundTruthGridVisibility;
    private boolean sourceDataGridVisibility;
    private boolean deepLearningDataGridVisibility;
    private Trajectory[] deepLearningResultCoords = null;
    private Trajectory[] deepLearningSourceCoords = null;
    private Trajectory[] deepLearningTargetCoords = null;
    private LineString[] deepLearningRawTrajectory = null;
    private LineString[] deepLearningNoiseTrajectory = null;

    CanvasPanel(IndoorMapmatchingSim parent) {
        gf = new GeometryFactory();
        indoorFeatures = new IndoorFeatures();
        relatedVisibilityEdge = new ArrayList<>();
        relatedD2DEdge = new ArrayList<>();
        circleBufferArray = new ArrayList<>();
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
        this.addKeyListener(this);
        this.parent = parent;
    }

    void setIndoorFeatures(IndoorFeatures indoorFeatures) {
        this.indoorFeatures = indoorFeatures;
    }

    void setTrajectory(LineString loadedTrajectory) {
        this.trajectory = loadedTrajectory;
        repaint();
    }

    void setTrajectory_IF(LineString indoorFilteredTrajectory) {
        trajectory_IF = indoorFilteredTrajectory;
        repaint();
    }

    void setTrajectory_GT(LineString groundTruthTrajectory) {
        trajectory_GT = groundTruthTrajectory;
        repaint();
    }

    IndoorFeatures getIndoorFeatures() {
        return indoorFeatures;
    }

    LineString getTrajectory() {
        return trajectory;
    }

    LineString getTrajectory_IF() {
        return trajectory_IF;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(2));
        g2.setFont(new Font("Serif", Font.PLAIN, 20));

        // draw mouse point coordinate
        // it related with mouseMoved function
        /*
        g2.setFont(new Font("Serif", Font.PLAIN, 20));
        g2.drawString((mousePositionX - SCREEN_BUFFER) + " , " + (mousePositionY - SCREEN_BUFFER), 20,20);
        if(trajectory != null)
            g2.drawString("Trajectory Index: " + (trajectory.getNumPoints() -1), 20, 40);
        */

        if(deepLearningResultCoords != null && deepLearningDataGridVisibility) {
            Coordinate[] coords = deepLearningResultCoords[dataIndex].getCoordinates();
            drawRect(g2, coords, 0.3f);
        }
        if(deepLearningSourceCoords != null && sourceDataGridVisibility) {
            Coordinate[] coords = deepLearningSourceCoords[dataIndex].getCoordinates();
            drawRect(g2, coords, 0.6f);
        }
        if(deepLearningTargetCoords != null && groundTruthGridVisibility) {
            Coordinate[] coords = deepLearningTargetCoords[dataIndex].getCoordinates();
            drawRect(g2, coords, 0f);
        }
        if(deepLearningRawTrajectory != null && rawTrajectoryVisibility) {
            LineString indexedRawTrajectory = deepLearningRawTrajectory[dataIndex];
            drawArrowLines(g2, indexedRawTrajectory.getCoordinates(), indexedRawTrajectory.getNumPoints(), Color.RED);
        }
        if(deepLearningNoiseTrajectory != null && noiseTrajectoryVisibility) {
            /*
            for(int i = 0; i < deepLearningNoiseTrajectory.length; i++) {
                LineString indexedRawTrajectory = deepLearningNoiseTrajectory[i];
                drawLines(g2, indexedRawTrajectory.getCoordinates(), Color.BLUE, false);
            }
             */
            LineString indexedNoiseTrajectory = deepLearningNoiseTrajectory[dataIndex];
            drawArrowLines(g2, indexedNoiseTrajectory.getCoordinates(), indexedNoiseTrajectory.getNumPoints(), Color.BLUE);
        }

        if(indoorFeatures != null) {
            int cellIndex = -1;
            for (CellSpace cellSpace : indoorFeatures.getCellSpaces()) {
                Polygon polygon = cellSpace.getGeom();
                cellIndex++;

                float drawPositionX = addScreenBuffer(polygon.getCentroid().getX()).floatValue();
                float drawPositionY = addScreenBuffer(polygon.getCentroid().getY()).floatValue();
                //Envelope internalEnvelope = polygon.getEnvelopeInternal();
                //float drawPositionX = addScreenBuffer(internalEnvelope.getMinX() + (internalEnvelope.getMaxX() - internalEnvelope.getMinX())/2).floatValue();
                //float drawPositionY = addScreenBuffer(internalEnvelope.getMinY() + (internalEnvelope.getMaxY() - internalEnvelope.getMinY())/2).floatValue();

                g2.setColor(Color.BLACK);
                //g2.drawString(String.valueOf(cellIndex), drawPositionX, drawPositionY);
                if(cellSpace.getLabel() != null) {
                    g2.setFont(new Font("TimesRoman", Font.PLAIN, 13));
                    if(!cellSpace.getLabel().equals("corridor"))
                        g2.drawString(cellSpace.getLabel(), drawPositionX - (cellSpace.getLabel().length())*3 , drawPositionY + 11);
                }

                if(currentEditStatus == EditStatus.SELECT_CELLSPACE && cellIndex == selectedCellIndex) {
                    drawLines(g2, polygon.getExteriorRing().getCoordinates(), Color.RED, true);
                    for(int i = 0; i < polygon.getNumInteriorRing(); i++) {
                        drawLines(g2, polygon.getInteriorRingN(i).getCoordinates(), Color.RED, true);
                    }
                }
                else {
                    drawLines(g2, polygon.getExteriorRing().getCoordinates(), Color.BLACK, false);
                    for(int i = 0; i < polygon.getNumInteriorRing(); i++) {
                        drawLines(g2, polygon.getInteriorRingN(i).getCoordinates(), Color.GRAY, true);
                    }
                }

                ArrayList<LineString> doors = cellSpace.getDoors();
                for (LineString lineString: doors) {
                    drawLines(g2, lineString.getCoordinates(), Color.YELLOW, false);
                }

                /*
                // draw door2door graph
                if(doors.size() > 1) {
                    ArrayList<LineString> d2dgraphs = cellSpace.getDoor2doorEdges();
                    for (LineString lineString: d2dgraphs) {
                        drawLines(g2, lineString.getCoordinates(), Color.RED, false);
                    }
                }
                */
                /*
                // draw visibility graph
                ArrayList<LineString> vgraph = cellSpace.getVisibilityEdges();
                for (LineString lineString: vgraph) {
                    drawLines(g2, lineString.getCoordinates(), Color.MAGENTA, false);
                }
                */
                if(currentEditStatus == EditStatus.GET_RELATED_EDGE) {
                    for (LineString lineString: relatedVisibilityEdge) {
                        drawLines(g2, lineString.getCoordinates(), Color.GREEN, false);
                    }
                    for (LineString lineString: relatedD2DEdge) {
                        drawLines(g2, lineString.getCoordinates(), Color.CYAN, false);
                    }
                }
            }
            parent.changeCanvasArea(ChangeCoord.getArea());
        }
        if(trajectory != null) {
            drawArrowLines(g2, trajectory.getCoordinates(), trajectory.getNumPoints(), Color.BLACK);
        }
        if(trajectory_IF != null) {
            drawArrowLines(g2, trajectory_IF.getCoordinates(), trajectory_IF.getNumPoints(), Color.BLUE);
        }
        if(trajectory_GT != null) {
            drawArrowLines(g2, trajectory_GT.getCoordinates(), trajectory_GT.getNumPoints(), Color.RED);
        }
        if(drawingCellCoords != null) {
            if(drawingCellCoords.length == 1) {
                drawPoint(g2, drawingCellCoords[0], Color.BLUE);
            }
            else {
                drawLines(g2, drawingCellCoords, Color.BLUE, true);
            }
        }
        if(trajectoryCoords != null) {
            drawPoint(g2, trajectoryCoords, Color.GREEN);
        }
    }

    private Double addScreenBuffer(double coord) {
        return coord + SCREEN_BUFFER;
    }

    private void drawPoint(Graphics2D g2, Coordinate coord, Color color) {
        g2.setColor(color);
        g2.draw(new Ellipse2D.Double(addScreenBuffer(coord.x - POINT_RADIUS), addScreenBuffer(coord.y - POINT_RADIUS), POINT_RADIUS * 2, POINT_RADIUS * 2));

    }

    private void drawLines(Graphics2D g2, Coordinate[] coords, Color color, Boolean isDrawPoint) {
        g2.setColor(color);
        if(coords.length > 1) {
            for(int i = 0; i < coords.length - 1; i++){
                g2.draw(new Line2D.Double(addScreenBuffer(coords[i].x), addScreenBuffer(coords[i].y), addScreenBuffer(coords[i+1].x), addScreenBuffer(coords[i+1].y)));
                if(isDrawPoint) {
                    g2.draw(new Ellipse2D.Double(addScreenBuffer(coords[i].x - POINT_RADIUS), addScreenBuffer(coords[i].y - POINT_RADIUS), POINT_RADIUS * 2, POINT_RADIUS * 2));
                }
            }
            if(isDrawPoint) {
                g2.draw(new Ellipse2D.Double(addScreenBuffer(coords[coords.length - 1].x - POINT_RADIUS), addScreenBuffer(coords[coords.length - 1].y - POINT_RADIUS), POINT_RADIUS * 2, POINT_RADIUS * 2));
            }
        }
    }

    private void drawRect(Graphics2D g2, Coordinate[] coords, float color) {
        HashMap<Coordinate, Float> gridCount = new HashMap<>();
        float maxCount = 1f;
        for(Coordinate coord: coords) {
            if(gridCount.containsKey(coord)){
                float count = gridCount.get(coord) + 1f;
                gridCount.remove(coord);
                gridCount.put(coord, count);
                if (count > maxCount) maxCount = count;
            }
            else {
                gridCount.put(coord, 1f);
            }
        }

        float remainSaturation = 1f / maxCount;
        for(Coordinate coord: coords) {
            float count = gridCount.get(coord);
            Color drawColor = Color.getHSBColor(color,  remainSaturation * count, 1f);
            g2.setColor(drawColor);
            g2.fillRect(addScreenBuffer(coord.getX()).intValue(), addScreenBuffer(coord.getY()).intValue(), DataUtils.GRID_SIZE, DataUtils.GRID_SIZE);
            g2.setColor(Color.BLACK);
            g2.drawRect(addScreenBuffer(coord.getX()).intValue(), addScreenBuffer(coord.getY()).intValue(), DataUtils.GRID_SIZE, DataUtils.GRID_SIZE);
        }
        gridCount.clear();
    }

    private void drawArrowLines(Graphics2D g2, Coordinate[] coords, int length, Color color) {
        if (coords.length > 1) {
            for (int i = 0; i < length - 1; i++) {
                drawArrowLine(g2, coords[i], coords[i+1], color);
            }
        }
    }

    private void drawArrowLine(Graphics2D g2, Coordinate startCoord, Coordinate endCoord, Color color) {
        g2.setColor(color);

        double x1 = addScreenBuffer(startCoord.x);
        double y1 = addScreenBuffer(startCoord.y);
        double x2 = addScreenBuffer(endCoord.x);
        double y2 = addScreenBuffer(endCoord.y);

        int dx = (int)(x2 - x1), dy = (int)(y2 - y1);
        double D = Math.sqrt(dx*dx + dy*dy);
        double xm = D - ARR_SIZE, xn = xm, ym = ARR_SIZE, yn = -ARR_SIZE, x;
        double sin = dy/D, cos = dx/D;

        x = xm*cos - ym*sin + x1;
        ym = xm*sin + ym*cos + y1;
        xm = x;

        x = xn*cos - yn*sin + x1;
        yn = xn*sin + yn*cos + y1;
        xn = x;

        int[] xpoints = {(int)x2, (int) xm, (int) xn};
        int[] ypoints = {(int)y2, (int) ym, (int) yn};

        g2.fillPolygon(xpoints, ypoints, 3);
        g2.setStroke(new BasicStroke(5, BasicStroke.CAP_ROUND, 0));
        g2.draw(new Line2D.Double(x1, y1, x2, y2));
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.requestFocus();
        int previousMouseX = e.getX() - (int) SCREEN_BUFFER;
        int previousMouseY = e.getY() - (int) SCREEN_BUFFER;

        Coordinate coord = new Coordinate(previousMouseX, previousMouseY);
        if (e.getButton() == 1) {
            if(currentEditStatus == null) {
                return;
            }
            else if(currentEditStatus == EditStatus.CREATE_CELLSPACE || currentEditStatus == EditStatus.CREATE_HOLE) {
                if (drawingCellCoords == null) {
                    drawingCellCoords = new Coordinate[1];
                    drawingCellCoords[0] = coord;
                }
                else {
                    if (drawingCellCoords[0].distance(coord) < SNAP_THRESHOLD) {
                        // if new_point is covered by first_point and SNAP_THRESHOLD buffer
                        // make polygon for cell space geometry
                        Coordinate newCoord = drawingCellCoords[0];
                        Coordinate[] realCellCoords = addCoordinate(newCoord);
                        CoordinateSequence seq = gf.getCoordinateSequenceFactory().create(realCellCoords);
                        LinearRing lr = gf.createLinearRing(seq);

                        if(currentEditStatus == EditStatus.CREATE_CELLSPACE) {
                            indoorFeatures.addCellSpace(new CellSpace(gf.createPolygon(lr)));
                        }
                        else if(currentEditStatus == EditStatus.CREATE_HOLE) {
                            selectedCellIndex = indoorFeatures.getCellSpaceIndex(new Coordinate(previousMouseX, previousMouseY))[0];
                            CellSpace cellSpace = indoorFeatures.getCellSpace(selectedCellIndex);
                            Polygon polygon = cellSpace.getGeom();
                            LinearRing exteriorRIng = gf.createLinearRing(polygon.getExteriorRing().getCoordinateSequence());
                            LinearRing[] interiorRings = new LinearRing[polygon.getNumInteriorRing() + 1];
                            for(int i = 0; i < polygon.getNumInteriorRing(); i++) {
                                interiorRings[i] = gf.createLinearRing(polygon.getInteriorRingN(i).getCoordinateSequence());
                            }
                            interiorRings[polygon.getNumInteriorRing()] = lr;
                            Polygon newGeom = gf.createPolygon(exteriorRIng, interiorRings);
                            cellSpace.setGeom(newGeom);
                        }

                        drawingCellCoords = null;
                    }
                    else {
                        Coordinate newCoord = new Coordinate(previousMouseX, previousMouseY);
                        drawingCellCoords = addCoordinate(newCoord);
                    }
                }
            }
            else if(currentEditStatus == EditStatus.CREATE_TRAJECTORY) {
                if(trajectoryCoords == null) {
                    trajectoryCoords = coord;
                    trajectory = null;
                }
                else {
                    trajectory = gf.createLineString(new Coordinate[]{trajectoryCoords, coord});
                    trajectory = indoorFeatures.getIndoorRoute(trajectory);
                    trajectoryCoords = null;
                }
            }
            else if(currentEditStatus == EditStatus.CREATE_DOOR) {
                selectedCellIndex = indoorFeatures.getCellSpaceIndex(new Coordinate(previousMouseX, previousMouseY))[0];
                Point clickedPosition = gf.createPoint(new Coordinate(previousMouseX,previousMouseY));
                Polygon polygon = indoorFeatures.getCellSpace(selectedCellIndex).getGeom();
                Coordinate[] lineStringArray = polygon.getExteriorRing().getCoordinates();

                double closestDistance = 10;
                for(int i = 0; i < lineStringArray.length - 1; i++) {
                    LineString tempLineString = gf.createLineString(new Coordinate[]{new Coordinate(lineStringArray[i].x, lineStringArray[i].y), new Coordinate(lineStringArray[i+1].x, lineStringArray[i+1].y)});
                    double tempDistance = tempLineString.distance(clickedPosition);

                    if(tempDistance < closestDistance) {
                        // TODO : Find closest point that lie on polygon
                    }
                }

            }
            else if(currentEditStatus == EditStatus.SELECT_CELLSPACE) {
                selectedCellIndex = indoorFeatures.getCellSpaceIndex(new Coordinate(previousMouseX, previousMouseY))[0];
            }
            else if(currentEditStatus == EditStatus.GET_RELATED_EDGE) {
                relatedVisibilityEdge.clear();
                relatedD2DEdge.clear();
                // find closest point
                Point targetPoint = null;
                for (CellSpace cellSpace : indoorFeatures.getCellSpaces()) {
                    if(targetPoint != null) break;

                    Polygon polygon = cellSpace.getGeom();
                    for(Coordinate geomCoord : polygon.getCoordinates()) {
                        if (geomCoord.distance(coord) < SNAP_THRESHOLD) {
                            targetPoint = gf.createPoint(geomCoord);
                            break;
                        }
                    }
                }
                // find visibility Edge and Door2Door Edge related with that point
                if(targetPoint != null) {
                    for (CellSpace cellSpace : indoorFeatures.getCellSpaces()) {
                        for(LineString lineString : cellSpace.getVisibilityEdges()){
                            if(lineString.getStartPoint().equals(targetPoint)) {
                                relatedVisibilityEdge.add(lineString);
                            }
                        }
                        for(LineString lineString : cellSpace.getDoor2doorEdges()){
                            if(lineString.getStartPoint().equals(targetPoint)) {
                                relatedD2DEdge.add(lineString);
                            }
                        }
                    }
                }
            }
        }
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

        mousePositionX = e.getX();
        mousePositionY = e.getY();
        //repaint();

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

    }

    private Coordinate[] addCoordinate(Coordinate newCoord) {
        int coordsNum = drawingCellCoords.length;
        Coordinate[] realCellCoords = new Coordinate[coordsNum + 1];
        for (int i = 0; i < coordsNum; i++) {
            realCellCoords[i] = new Coordinate(drawingCellCoords[i].x, drawingCellCoords[i].y);
        }
        realCellCoords[coordsNum] = new Coordinate(newCoord.x, newCoord.y);
        return realCellCoords;
    }

    static Geometry getSubLineString(LineString lineString, int size) {
        return getSubLineString(lineString, 0, size);
    }

    static Geometry getSubLineString(LineString lineString, int startIndex, int size) {
        GeometryFactory gf = new GeometryFactory();
        if(size > lineString.getNumPoints()) return lineString;
        else if(startIndex > lineString.getNumPoints()) return null;
        else if(size == 1) return gf.createPoint(lineString.getCoordinateN(0));
        else {
            if(startIndex + size > lineString.getNumPoints())
                size = lineString.getNumPoints() - startIndex;
            Coordinate[] originalCoords = lineString.getCoordinates();
            Coordinate[] newCoords = new Coordinate[size];

            System.arraycopy(originalCoords, startIndex, newCoords, 0, size);

            return gf.createLineString(newCoords);
        }
    }

    void syntheticTrajectoryTest(JTextPane textPaneOriginal) {
        ArrayList<TimeTableElement> timeTableElements = new ArrayList<>();
        Random random = new Random();
        int trajectory_num = 10000;
        int validation_num = (int) (trajectory_num * 0.3);
        int timeTableElementsNum = 3;
        int travelTimeUpperBound = 20;
        int travelTimeLowerBound = 5;
        int cellSpacesCount = indoorFeatures.getCellSpaces().size();
        int stepCount = 100;

        try {
            FileWriter fw_rt    = new FileWriter("raw_trajectory.txt");
            FileWriter fw_nt    = new FileWriter("noise_trajectory.txt");
            FileWriter fw_gt_train    = new FileWriter("tgt-train.txt");
            FileWriter fw_noise_train = new FileWriter("src-train.txt");
            FileWriter fw_gt_validation    = new FileWriter("tgt-val.txt");
            FileWriter fw_noise_validation = new FileWriter("src-val.txt");

            PrintWriter pw_rt   = new PrintWriter(fw_rt);
            PrintWriter pw_nt   = new PrintWriter(fw_nt);
            PrintWriter pw_gt_train     = new PrintWriter(fw_gt_train);
            PrintWriter pw_noise_train  = new PrintWriter(fw_noise_train);
            PrintWriter pw_gt_validation     = new PrintWriter(fw_gt_validation);
            PrintWriter pw_noise_validation  = new PrintWriter(fw_noise_validation);

            for (int t = 0; t < trajectory_num; t++) {

                for (int i = 0; i < timeTableElementsNum; i++) {
                    int startCellIndex = random.nextInt(cellSpacesCount);
                    int endCellIndex = random.nextInt(cellSpacesCount);
                    int travelTime = random.nextInt(travelTimeUpperBound- travelTimeLowerBound) + travelTimeLowerBound;
                    timeTableElements.add(new TimeTableElement(startCellIndex, endCellIndex, travelTime));
                }

                SyntheticTrajectoryGenerator generator = new SyntheticTrajectoryGenerator(indoorFeatures);
                SyntheticDataElement syntheticData = generator.generate(timeTableElements);

                for (int i = 0; i < syntheticData.getNoiseTrajectories().size(); i++) {
                    StringBuilder raw_trajectory   = new StringBuilder();
                    StringBuilder noise_trajectory = new StringBuilder();
                    StringBuilder ground_truth     = new StringBuilder();
                    StringBuilder noise_result     = new StringBuilder();
                    for (int j = 0; j < syntheticData.getGroundTruthTrajectory_gridLayout().length; j++) {
                        raw_trajectory.append(syntheticData.getRawTrajectory().getCoordinateN(j).getX()).append("_").append(syntheticData.getRawTrajectory().getCoordinateN(j).getY()).append(" ");
                        noise_trajectory.append(syntheticData.getNoiseTrajectories().get(i).getCoordinateN(j).getX()).append("_").append(syntheticData.getNoiseTrajectories().get(i).getCoordinateN(j).getY()).append(" ");
                        ground_truth.append(syntheticData.getGroundTruth_SIMM()[j]).append("@").append(syntheticData.getGroundTruthTrajectory_gridLayout()[j]).append(" ");
                        noise_result.append(syntheticData.getNoise_SIMM().get(i)[j]).append("@").append(syntheticData.getNoiseTrajectories_gridLayout().get(i)[j]).append(" ");
                    }
                    pw_rt.println(raw_trajectory);
                    pw_nt.println(noise_trajectory);
                    if(t < validation_num) {
                        pw_gt_validation.println(ground_truth + ".");
                        pw_noise_validation.println(noise_result + ".");
                    }
                    else {
                        pw_gt_train.println(ground_truth + ".");
                        pw_noise_train.println(noise_result + ".");
                    }

                    if(t == 0) {
                        trajectory = syntheticData.getRawTrajectory();
                        trajectory_IF = syntheticData.getNoiseTrajectories().get(0);
                        repaint();
                    }
                }
                timeTableElements.clear();
                if(t % stepCount == 0)
                    System.out.println("Step: " + t + "/" + trajectory_num);
            }

            pw_rt.close();
            pw_nt.close();
            pw_gt_train.close();
            pw_noise_train.close();
            pw_gt_validation.close();
            pw_noise_validation.close();
            System.out.println("Generate end!");
        } catch (IOException e) {
            e.printStackTrace();
        }

        /* Print log
        trajectory = syntheticData.getRawTrajectory();
        trajectory_IF = syntheticData.getNoiseTrajectories().get(0);
        for(int groundTruth : syntheticData.getGround_truth()) {
            String originalText = textPaneOriginal.getText();
            textPaneOriginal.setText(originalText + groundTruth + " (" + indoorFeatures.getCellSpaceLabel(groundTruth) + ")\n");
        }
        repaint();
        */
    }

    void saveImage(String fileID) {
        //BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        BufferedImage image = new BufferedImage(1024, 768, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = image.createGraphics();
        paint(g2);

        // Check a directory is exist or not
        File dir = new File(resultPath);
        if(!dir.exists()){
            dir.mkdirs();
        }

        // Save CanvasPanel as image
        try {
            ImageIO.write(image, "PNG", new File(resultPath + "screenshot_" + fileID + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void doIndoorMapMatching(JTextPane textPaneOriginal) {
        StringBuilder result = new StringBuilder();
        if(indoorFeatures != null && trajectory != null) {
            int windowSize = 5;
            double radius = 3 * ChangeCoord.CANVAS_MULTIPLE;
            DirectIndoorMapMatching dimm = new DirectIndoorMapMatching(indoorFeatures);
            HMMIndoorMapMatching himm = new HMMIndoorMapMatching(indoorFeatures);

            result.append("==original trajectory==\n");
            result.append("==DIMM==\n");
            printMapMatchingResults(result, trajectory, dimm.getMapMatchingResult(trajectory));
            result.append("==HIMM==\n");

            himm.setInitialProbability(trajectory.getPointN(0));
            himm.makeAMatrixByTopology();
            //himm.makeAMatrixByDistance();
            //himm.makeBMatrixCellBuffer(radius);
            String[] mapMatchingResults = getResultHIMMCell(himm, trajectory, radius, windowSize);
            //String[] mapMatchingResults = getResultHIMMCircleBuffer(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE* 3, windowSize);
            printMapMatchingResults(result, trajectory, mapMatchingResults);

            LineString lineWithMaxIndoorDistance;
            try {
                lineWithMaxIndoorDistance = IndoorUtils.applyIndoorDistanceFilter(trajectory, MAX_DISTANCE, indoorFeatures);
                setTrajectory_IF(lineWithMaxIndoorDistance);
            } catch (Exception e) {
                e.printStackTrace();
            }

            result.append("==Indoor distance filtered trajectory==\n");
            result.append("==DIMM==\n");
            printMapMatchingResults(result, trajectory, dimm.getMapMatchingResult(trajectory_IF));
            result.append("==HIMM==\n");
            himm.makeAMatrixByTopology();
            //himm.makeAMatrixByDistance();
            mapMatchingResults = getResultHIMMCell(himm, trajectory_IF, ChangeCoord.CANVAS_MULTIPLE, windowSize);
            //mapMatchingResults = getResultHIMMCircleBuffer(himm, trajectory_IF, ChangeCoord.CANVAS_MULTIPLE);
            printMapMatchingResults(result, trajectory, mapMatchingResults);

            textPaneOriginal.setText(result.toString());

            /*
            System.out.println(result);
            for(int i = 0; i < trajectory.getNumPoints(); i++){
                Coordinate coord = trajectory.getCoordinateN(i);
                Coordinate coord_IF = trajectory_IF.getCoordinateN(i);
                System.out.printf("%d th point\n", i);
                System.out.printf("Point Coord (%f,%f) : %s\n", coord.x, coord.y, dimm.getMapMatchingResult(trajectory)[i]);
                System.out.printf("IF Point Coord (%f,%f) : %s\n", coord_IF.x, coord_IF.y, dimm.getMapMatchingResult(trajectory_IF)[i]);
                System.out.println();
            */
        }
    }

    void getGroundTruthResult(JTextPane textPaneOriginal, String[] groundTruth) {
        StringBuilder result = new StringBuilder();
        if(indoorFeatures != null && groundTruth != null) {
            result.append("==Ground truth==\n");
            printMapMatchingResults(result, trajectory, groundTruth);
        }
        String originalText = textPaneOriginal.getText();
        textPaneOriginal.setText(originalText + result.toString());
    }

    public static void printMapMatchingResults(StringBuilder result, LineString trajectory, String[] mapMatchingResult) {
        if(mapMatchingResult != null) {
            for(int i = 0; i < trajectory.getNumPoints(); i++){
                result.append(i).append("th: ").append(mapMatchingResult[i]).append("\n");
            }
        }
    }

    ExperimentResult evaluateSIMM_Excel(String fileID, ArrayList<String> keyList, String[] stringGT) {
        final int BUFFER_REPETITION = 5;
        ExperimentResult experimentResult = new ExperimentResult(fileID);
        Workbook workbook = new XSSFWorkbook();
        if(indoorFeatures != null && trajectory != null) {
            DirectIndoorMapMatching dimm = new DirectIndoorMapMatching(indoorFeatures);
            HMMIndoorMapMatching himm    = new HMMIndoorMapMatching(indoorFeatures);
            Sheet resultSheet = workbook.createSheet("Result");

            // Make SIMM result from original trajectory
            Row resultSheetRow = resultSheet.createRow(0);
            resultSheetRow.createCell(0).setCellValue("original trajectory");
            getEvaluateResults(BUFFER_REPETITION, dimm, himm, trajectory, resultSheet);
            System.out.println("original trajectory end");

            // Make SIMM result from indoor distance filtered trajectory
            LineString lineWithMaxIndoorDistance;
            try {
                lineWithMaxIndoorDistance = IndoorUtils.applyIndoorDistanceFilter(trajectory, MAX_DISTANCE, indoorFeatures);
                setTrajectory_IF(lineWithMaxIndoorDistance);
            } catch (Exception e) {
                e.printStackTrace();
            }
            resultSheetRow = resultSheet.createRow(resultSheet.getLastRowNum() + 1);
            resultSheetRow.createCell(0).setCellValue("Indoor distance filtered trajectory");
            getEvaluateResults(BUFFER_REPETITION, dimm, himm, trajectory_IF, resultSheet);
            System.out.println("Indoor distance filtered trajectory end");

            // Make ground truth result using DIMM and evaluate all result
            if(keyList.isEmpty()) {
                getGroundTruthResult_Excel(workbook, experimentResult, keyList, stringGT);
            } else {
                getGroundTruthResult_Excel(workbook, experimentResult, null, stringGT);
            }

            // Check a directory is exist or not
            File dir = new File(resultPath);
            if(!dir.exists()){
                dir.mkdirs();
            }

            // Write all result in a excel file
            FileOutputStream outFile;
            try {
                outFile = new FileOutputStream(resultPath + "Result_" + fileID + ".xlsx");
                workbook.write(outFile);
                outFile.close();
                System.out.println("Result_" + fileID + " write complete");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return experimentResult;
    }

    private void getEvaluateResults(int BUFFER_REPETITION, DirectIndoorMapMatching dimm, HMMIndoorMapMatching himm, LineString trajectory, Sheet sheet) {
        Row row;
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("DIMM");
        for(int i = 0; i < trajectory.getNumPoints(); i++){
            row.createCell(i+1).setCellValue(dimm.getMapMatchingResult(trajectory)[i]);
        }

        final double DEFAULT_RADIUS = 0.5;
        final int DEFAULT_WINDOW = 2;
        final int WINDOW_REPETITION = 20;
        double radius;
        int windowSize;
        // A matrix : based on topology graph
        // B matrix : based on Cell buffer
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM, A matrix : Topology graph / B matrix : Cell buffer");
        radius = DEFAULT_RADIUS;

        himm.makeAMatrixByTopology();
        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = DEFAULT_WINDOW;

            for(int j = 0; j < WINDOW_REPETITION; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMCell(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += DEFAULT_WINDOW;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMCell(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints());

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += DEFAULT_RADIUS;
        }
        System.out.println("HIMM, A matrix : Topology graph / B matrix : Cell buffer end");

        // A matrix : based on graph distance
        // B matrix : based on Cell buffer
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM, A matrix : Graph distance / B matrix : Cell buffer");
        radius = DEFAULT_RADIUS;

        himm.makeAMatrixByGraphDistance();
        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = DEFAULT_WINDOW;

            for(int j = 0; j < WINDOW_REPETITION; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMCell(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += DEFAULT_WINDOW;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMCell(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints());

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += DEFAULT_RADIUS;
        }
        System.out.println("HIMM, A matrix : Graph distance / B matrix : Cell buffer end");

        // A matrix : based on MO status
        // B matrix : based on Cell buffer
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM, A matrix : MO status / B matrix : Cell buffer");
        final double incrementalValue = 0.1;
        radius = DEFAULT_RADIUS;

        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = DEFAULT_WINDOW;

            for (int j = 0; j < WINDOW_REPETITION; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMMOStatusWithCell(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize, MIN_DISTANCE, incrementalValue);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += DEFAULT_WINDOW;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMMOStatusWithCircle(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints(), MIN_DISTANCE, incrementalValue);

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += DEFAULT_RADIUS;
        }
        System.out.println("HIMM, A matrix : MO status P / B matrix : Cell buffer end");

        // A matrix : based on topology graph
        // B matrix : based on circle buffer
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM, A matrix : Topology graph / B matrix : Circle buffer");
        radius = DEFAULT_RADIUS;

        himm.makeAMatrixByTopology();
        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = DEFAULT_WINDOW;

            for(int j = 0; j < WINDOW_REPETITION; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMCircleBuffer(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += DEFAULT_WINDOW;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMCircleBuffer(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints());

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += DEFAULT_RADIUS;
        }
        System.out.println("HIMM, A matrix : Topology graph / B matrix : Circle buffer end");

        // A matrix : based on graph distance
        // B matrix : based on circle buffer
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM, A matrix : Graph distance / B matrix : Circle buffer");
        radius = DEFAULT_RADIUS;

        himm.makeAMatrixByGraphDistance();
        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = DEFAULT_WINDOW;

            for(int j = 0; j < WINDOW_REPETITION; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMCircleBuffer(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += DEFAULT_WINDOW;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMCircleBuffer(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints());

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += DEFAULT_RADIUS;
        }
        System.out.println("HIMM, A matrix : Graph distance / B matrix : Circle buffer end");

        // A matrix : based on MO status
        // B matrix : based on circle buffer
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM, A matrix : MO status / B matrix : Circle buffer");
        radius = DEFAULT_RADIUS;

        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = DEFAULT_WINDOW;

            for (int j = 0; j < WINDOW_REPETITION; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMMOStatusWithCircle(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize, MIN_DISTANCE, incrementalValue);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += DEFAULT_WINDOW;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMMOStatusWithCircle(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints(), MIN_DISTANCE, incrementalValue);

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += DEFAULT_RADIUS;
        }
        System.out.println("HIMM, A matrix : MO status P / B matrix : Circle buffer end");
    }

    private void setMatMatchingResultToRow(Row row, String[] mapMatchingResults) {
        if(mapMatchingResults != null) {
            for(int index = 0; index < mapMatchingResults.length; index++){
                row.createCell(index+1).setCellValue(mapMatchingResults[index]);
            }
        }
        else {
            row.createCell(1).setCellValue("Impossible");
            System.out.println("impossible");
        }
    }

    private String[] getResultHIMMCell(HMMIndoorMapMatching himm, LineString trajectory, double radius, int windowSize) {
        ArrayList<Integer> observationArrayList =  new ArrayList<>();
        himm.clearOnlyBMatrix();
        himm.makeBMatrixCellBuffer(radius);
        himm.setCircleSize(radius);

        for(int i = 0; i < trajectory.getNumPoints(); i++) {
            double candidateBufferRadius = radius;
            if(i != 0) {
                candidateBufferRadius = trajectory.getPointN(i - 1).distance(trajectory.getPointN(i));
            }
            if(candidateBufferRadius < radius) {
                candidateBufferRadius = radius;
            }
            himm.makeBMatrixCellBuffer(trajectory.getCoordinateN(i), candidateBufferRadius, windowSize);

            int mapMatchingResult;
            if(i > windowSize) {
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory,i - windowSize, windowSize), candidateBufferRadius, observationArrayList.subList(i - windowSize, i));
            }
            else {
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory,0, i), candidateBufferRadius, observationArrayList.subList(0,i));
            }
            observationArrayList.add(mapMatchingResult);
        }

        String[] mapMatchingResults = new String[observationArrayList.size()];
        for(int i = 0; i < observationArrayList.size(); i++) {
            mapMatchingResults[i] = himm.getLable(observationArrayList.get(i));
        }
        return mapMatchingResults;
    }

    private String[] getResultHIMMCircleBuffer(HMMIndoorMapMatching himm, LineString trajectory, double radius, int windowSize) {
        ArrayList<Integer> observationArrayList =  new ArrayList<>();
        himm.clearOnlyBMatrix();
        himm.setCircleSize(radius);

        for(int i = 0; i < trajectory.getNumPoints(); i++) {
            double candidateBufferRadius = radius;
            if(i != 0) {
                candidateBufferRadius = trajectory.getPointN(i - 1).distance(trajectory.getPointN(i));
            }
            if(candidateBufferRadius < radius) {
                candidateBufferRadius = radius;
            }
            himm.makeBMatrixCircleBuffer(trajectory.getCoordinateN(i), candidateBufferRadius, windowSize);

            // 초기확률 matrix 설정 및 맵 매칭 수행
            int mapMatchingResult;
            if (i > windowSize) {   // 궤적의 길이가 window 크기 이상인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, i - windowSize, windowSize), candidateBufferRadius, observationArrayList.subList(i - windowSize, i));
            } else {                // 궤적의 길이가 window 크기 미만인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, 0, i), candidateBufferRadius, observationArrayList.subList(0,i));
            }
            observationArrayList.add(mapMatchingResult);
        }

        String[] mapMatchingResults = new String[observationArrayList.size()];
        for(int i = 0; i < observationArrayList.size(); i++) {
            mapMatchingResults[i] = himm.getLable(observationArrayList.get(i));
        }
        return mapMatchingResults;
    }

    private String[] getResultHIMMMOStatusWithCell(HMMIndoorMapMatching himm, LineString trajectory, double radius, int windowSize, double MIN_DISTANCE, double incrementalValue) {
        ArrayList<Integer> observationArrayList =  new ArrayList<>();
        himm.clearOnlyBMatrix();
        himm.makeBMatrixCellBuffer(radius);
        himm.setCircleSize(radius);

        double sigma = 0.5;
        for(int i = 0; i < trajectory.getNumPoints(); i++) {
            // A matrix 설정
            double movingDistance;
            if(i != 0) {
                movingDistance = trajectory.getPointN(i - 1).distance(trajectory.getPointN(i));
                if(movingDistance < MIN_DISTANCE) { // Moving object status = staying
                    sigma = Math.max(sigma + incrementalValue, 1);
                }
                else {  // Moving object status = moving
                    sigma = Math.min(sigma - incrementalValue, 0);
                }
            }
            himm.makeAMatrixByStaticP(sigma);

            // B matrix 설정
            double candidateBufferRadius = radius;
            if(i != 0) {
                candidateBufferRadius = trajectory.getPointN(i - 1).distance(trajectory.getPointN(i));
            }
            if(candidateBufferRadius < radius) {
                candidateBufferRadius = radius;
            }
            himm.makeBMatrixCellBuffer(trajectory.getCoordinateN(i), candidateBufferRadius, windowSize);

            // 초기확률 matrix 설정 및 맵 매칭 수행
            int mapMatchingResult;
            if (i > windowSize) {   // 궤적의 길이가 window 크기 이상인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, i - windowSize, windowSize), candidateBufferRadius, observationArrayList.subList(i - windowSize, i));
            } else {    // 궤적의 길이가 window 크기 미만인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, 0, i), candidateBufferRadius, observationArrayList.subList(0,i));
            }
            observationArrayList.add(mapMatchingResult);
        }

        // 전체 궤적에 대한 결과 생성
        String[] mapMatchingResults = new String[observationArrayList.size()];
        for(int i = 0; i < observationArrayList.size(); i++) {
            mapMatchingResults[i] = himm.getLable(observationArrayList.get(i));
        }
        return mapMatchingResults;
    }

    private ExperimentResult getGroundTruthResult_Excel(Workbook workbook, ExperimentResult er, ArrayList<String> keyList, String[] stringGT) {
        Sheet resultSheet = workbook.getSheet("Result");
        Sheet summarySheet = workbook.createSheet("Summary");

        int numTrPoint = trajectory.getNumPoints();
        er.numTrajectoryPoint = numTrPoint;
        er.trajectoryLength[0] = trajectory.getLength() / ChangeCoord.CANVAS_MULTIPLE;
        er.trajectoryLength[1] = trajectory_IF.getLength() / ChangeCoord.CANVAS_MULTIPLE;
        if(trajectory_GT != null) {
            er.trajectoryLength[2] = trajectory_GT.getLength() / ChangeCoord.CANVAS_MULTIPLE;

            double[] errorAvg = new double[2];
            double[] distance_original_GT = new double[numTrPoint];
            double[] distance_IF_GT = new double[numTrPoint];
            for(int i = 0; i < numTrPoint; i++){
                distance_original_GT[i] = trajectory_GT.getPointN(i).distance(trajectory.getPointN(i)) / ChangeCoord.CANVAS_MULTIPLE;
                distance_IF_GT[i] = trajectory_GT.getPointN(i).distance(trajectory_IF.getPointN(i)) / ChangeCoord.CANVAS_MULTIPLE;
                errorAvg[0] += distance_original_GT[i];
                errorAvg[1] += distance_IF_GT[i];
            }
            er.averageError[0] = errorAvg[0] / numTrPoint;
            er.averageError[1] = errorAvg[1] / numTrPoint;

            double[] errorVar = new double[2];
            for(int i = 0; i < numTrPoint; i++){
                errorVar[0] += Math.pow(distance_original_GT[i] - er.averageError[0],2);
                errorVar[1] += Math.pow(distance_IF_GT[i] - er.averageError[1],2);
            }
            er.varianceError[0] = errorVar[0] / numTrPoint;
            er.varianceError[1] = errorVar[1] / numTrPoint;
        }

        Row resultSheetRow = resultSheet.createRow(resultSheet.getLastRowNum() + 1);
        resultSheetRow.createCell(0).setCellValue("Ground Truth");
        for(int i = 0; i < numTrPoint; i++){
            resultSheetRow.createCell(i+1).setCellValue(stringGT[i]);
        }

        String bigClass = null, middleClass = null;
        for(int i = 0; i < resultSheet.getLastRowNum(); i++) {
            resultSheetRow = resultSheet.getRow(i);
            if(resultSheetRow.getLastCellNum() != 1) {
                Row summaryRow = summarySheet.createRow(i);
                double countTrue = 0;
                for(int j = 1; j < resultSheetRow.getLastCellNum(); j++) {
                    if(resultSheetRow.getCell(j).getStringCellValue().equals(stringGT[j-1])) {
                        summaryRow.createCell(j).setCellValue(true);
                        countTrue++;
                    }
                    else {
                        summaryRow.createCell(j).setCellValue(false);
                    }
                }
                summaryRow.createCell(0).setCellValue(countTrue);
                double accuracy = countTrue / (resultSheetRow.getLastCellNum() - 1) * 100;
                summaryRow.createCell(summaryRow.getLastCellNum()).setCellValue(accuracy);

                String key = bigClass + "_" + middleClass + "_" + resultSheetRow.getCell(0).getStringCellValue();
                er.accuracy.put(key, accuracy);
                er.trueCount.put(key, countTrue);
                if(keyList != null)
                    keyList.add(key);
            }
            else {
                String className = resultSheetRow.getCell(0).getStringCellValue();
                if(className.equals("original trajectory") || className.equals("Indoor distance filtered trajectory")){
                    bigClass = className;
                }
                else {
                    middleClass = className;
                }
            }
        }

        Row summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Metric");
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Number of points:");
        summaryRow.createCell(1).setCellValue(er.numTrajectoryPoint);
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Length of original trajectory:");
        summaryRow.createCell(1).setCellValue(er.trajectoryLength[0]);
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Length of indoor filtered trajectory:");
        summaryRow.createCell(1).setCellValue(er.trajectoryLength[1]);
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Length of ground truth trajectory:");
        summaryRow.createCell(1).setCellValue(er.trajectoryLength[2]);
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Average Error of original trajectory:");
        summaryRow.createCell(1).setCellValue(er.averageError[0]);
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Average Error of indoor filtered trajectory:");
        summaryRow.createCell(1).setCellValue(er.averageError[1]);
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Variance Error of original trajectory:");
        summaryRow.createCell(1).setCellValue(er.varianceError[0]);
        summaryRow = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
        summaryRow.createCell(0).setCellValue("Variance Error of indoor filtered trajectory:");
        summaryRow.createCell(1).setCellValue(er.varianceError[1]);

        return er;
    }

// SIG 용 함수 시작
    private String[] getResultHIMMBasic(HMMIndoorMapMatching himm, LineString trajectory, double radius, int windowSize) {
        ArrayList<Integer> observationArrayList =  new ArrayList<>();
        himm.clearOnlyBMatrix();
        himm.setCircleSize(radius);

        for(int i = 0; i < trajectory.getNumPoints(); i++) {
            // B matrix 설정
            himm.makeBMatrixCircleBuffer(trajectory.getCoordinateN(i), radius, windowSize);

            // 초기확률 matrix 설정 및 맵 매칭 수행
            int mapMatchingResult;
            if (i > windowSize) {   // 궤적의 길이가 window 크기 이상인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, i - windowSize, windowSize), radius, observationArrayList.subList(i - windowSize, i));
            } else {                // 궤적의 길이가 window 크기 미만인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, 0, i), radius, observationArrayList.subList(0,i));
            }
            observationArrayList.add(mapMatchingResult);
        }

        // 전체 궤적에 대한 결과 생성
        String[] mapMatchingResults = new String[observationArrayList.size()];
        for(int i = 0; i < observationArrayList.size(); i++) {
            mapMatchingResults[i] = himm.getLable(observationArrayList.get(i));
        }
        return mapMatchingResults;
    }

    private String[] getResultHIMMMOStatusWithCircle(HMMIndoorMapMatching himm, LineString trajectory, double radius, int windowSize, double MIN_DISTANCE, double incrementalValue) {
        ArrayList<Integer> observationArrayList =  new ArrayList<>();
        himm.clearOnlyBMatrix();
        himm.setCircleSize(radius);

        double sigma = 0.5;
        for(int i = 0; i < trajectory.getNumPoints(); i++) {
            // A matrix 설정
            double movingDistance;
            if(i != 0) {
                movingDistance = trajectory.getPointN(i - 1).distance(trajectory.getPointN(i));
                if(movingDistance < MIN_DISTANCE) { // Moving object status = staying
                    sigma = Math.max(sigma + incrementalValue, 1);
                }
                else {  // Moving object status = moving
                    sigma = Math.min(sigma - incrementalValue, 0);
                }
            }
            himm.makeAMatrixByStaticP(sigma);

            // B matrix 설정
            double bufferRadius = radius;
            if(i != 0) {
                bufferRadius = trajectory.getPointN(i - 1).distance(trajectory.getPointN(i));
            }
            if(bufferRadius < radius) {
                bufferRadius = radius;
            }
            himm.makeBMatrixCircleBuffer(trajectory.getCoordinateN(i), bufferRadius, windowSize);

            // 초기확률 matrix 설정 및 맵 매칭 수행
            int mapMatchingResult;
            if (i > windowSize) {   // 궤적의 길이가 window 크기 이상인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, i - windowSize, windowSize), bufferRadius, observationArrayList.subList(i - windowSize, i));
            } else {    // 궤적의 길이가 window 크기 미만인 경우
                mapMatchingResult = himm.getMapMatchingResult(trajectory.getPointN(i), getSubLineString(trajectory, 0, i), bufferRadius, observationArrayList.subList(0,i));
            }
            observationArrayList.add(mapMatchingResult);
        }

        // 전체 궤적에 대한 결과 생성
        String[] mapMatchingResults = new String[observationArrayList.size()];
        for(int i = 0; i < observationArrayList.size(); i++) {
            mapMatchingResults[i] = himm.getLable(observationArrayList.get(i));
        }
        return mapMatchingResults;
    }

    private void getEvaluateResults_forSIG(int BUFFER_REPETITION, DirectIndoorMapMatching dimm, HMMIndoorMapMatching himm, LineString trajectory, Sheet sheet) {
        Row row;
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("DIMM");
        for(int i = 0; i < trajectory.getNumPoints(); i++){
            row.createCell(i+1).setCellValue(dimm.getMapMatchingResult(trajectory)[i]);
        }

        double radius;
        int windowSize;
        // Basic methods
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM Basic method");
        radius = 0.5;
        // A matrix 설정
        himm.makeAMatrixByTopology();
        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = 10;
            for (int j = 0; j < 5; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMBasic(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += 10;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMBasic(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints());

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += 0.5;
        }
        System.out.println("HIMM Basic method end");

        // Using moving object status methods
        final double incrementalValue = 0.1;
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue("HIMM using MO status");
        radius = 0.5;
        for(int i = 0; i < BUFFER_REPETITION; i++) {
            windowSize = 10;
            for (int j = 0; j < 5; j++) {
                row = sheet.createRow(sheet.getLastRowNum() + 1);
                row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: " + windowSize);

                String[] mapMatchingResults = getResultHIMMMOStatusWithCircle(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, windowSize, MIN_DISTANCE, incrementalValue);

                setMatMatchingResultToRow(row, mapMatchingResults);
                windowSize += 10;
            }
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            row.createCell(0).setCellValue("Radius(m): " + radius + " / Window: MAX");

            String[] mapMatchingResults = getResultHIMMMOStatusWithCircle(himm, trajectory, ChangeCoord.CANVAS_MULTIPLE * radius, trajectory.getNumPoints(), MIN_DISTANCE, incrementalValue);

            setMatMatchingResultToRow(row, mapMatchingResults);
            radius += 0.5;
        }
        System.out.println("HIMM using MO status end");
    }

    ExperimentResult evaluateSIMM_forSIG(String fileID, ArrayList<String> keyList, String[] stringGT) {
        final int BUFFER_REPETITION = 10;
        ExperimentResult experimentResult = new ExperimentResult(fileID);
        Workbook workbook = new XSSFWorkbook();
        if(indoorFeatures != null && trajectory != null) {
            DirectIndoorMapMatching dimm = new DirectIndoorMapMatching(indoorFeatures);
            HMMIndoorMapMatching himm = new HMMIndoorMapMatching(indoorFeatures);
            Sheet resultSheet = workbook.createSheet("Result");

            // Make SIMM result from original trajectory
            Row resultSheetRow = resultSheet.createRow(0);
            resultSheetRow.createCell(0).setCellValue("original trajectory");

            getEvaluateResults_forSIG(BUFFER_REPETITION, dimm, himm, trajectory, resultSheet);
            System.out.println("original trajectory end");

            // Make SIMM result from indoor distance filtered trajectory
            LineString lineWithMaxIndoorDistance;
            try {
                lineWithMaxIndoorDistance = IndoorUtils.applyIndoorDistanceFilter(trajectory, MAX_DISTANCE, indoorFeatures);
                setTrajectory_IF(lineWithMaxIndoorDistance);
            } catch (Exception e) {
                e.printStackTrace();
            }
            resultSheetRow = resultSheet.createRow(resultSheet.getLastRowNum() + 1);
            resultSheetRow.createCell(0).setCellValue("Indoor distance filtered trajectory");

            getEvaluateResults_forSIG(BUFFER_REPETITION, dimm, himm, trajectory_IF, resultSheet);
            System.out.println("Indoor distance filtered trajectory end");

            // Make ground truth result using DIMM and evaluate all result
            if(keyList.isEmpty()) {
                getGroundTruthResult_Excel(workbook, experimentResult, keyList, stringGT);
            } else {
                getGroundTruthResult_Excel(workbook, experimentResult, null, stringGT);
            }

            // Check a directory is exist or not
            File dir = new File(resultPath);
            if(!dir.exists()){
                dir.mkdirs();
            }

            // Write all result in a excel file
            FileOutputStream outFile;
            try {
                outFile = new FileOutputStream(resultPath + "Result_" + fileID + ".xlsx");
                workbook.write(outFile);
                outFile.close();
                System.out.println("Result_" + fileID + " write complete");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return experimentResult;
    }
    // SIG용 함수 끝

    public void setDeepLearningResults(Trajectory[] deepLearningGridResult, Trajectory[] sourceGridResult, Trajectory[] targetGridResult, LineString[] rawTrajectories, LineString[] noiseTrajectories) {
        deepLearningResultCoords    = deepLearningGridResult.clone();
        deepLearningSourceCoords    = sourceGridResult.clone();
        deepLearningTargetCoords    = targetGridResult.clone();
        deepLearningRawTrajectory   = rawTrajectories.clone();
        deepLearningNoiseTrajectory = noiseTrajectories.clone();

        int coordsNum = 0;
        int validSIMMCount = 0;
        int validDLCount = 0;

        for(int i = 0; i < deepLearningTargetCoords.length; i++) {
            String[] groundTruth = deepLearningTargetCoords[i].getLabels();
            String[] SIMM_Result = deepLearningSourceCoords[i].getLabels();
            String[] DL_Results  = deepLearningResultCoords[i].getLabels();
            coordsNum += groundTruth.length;
            for(int j = 0; j < groundTruth.length; j++) {
                if(groundTruth[j].equals(SIMM_Result[j])) {
                    validSIMMCount += 1;
                }
                if(j < DL_Results.length && groundTruth[j].equals(DL_Results[j])) {
                    validDLCount += 1;
                }
            }
            setDataIndex(i);
        }

        float accuracy_SIMM = (float) validSIMMCount / coordsNum * 100;
        float accuracy_DL = (float) validDLCount / coordsNum * 100;
        System.out.println("All coords #:" + coordsNum);
        System.out.println("SIMM Average Accuracy:" + accuracy_SIMM);
        System.out.println("SIMM Min Accuracy:" + min_SIMM_Acc);
        System.out.println("SIMM Max Accuracy:" + max_SIMM_Acc);
        System.out.println("DL Average Accuracy:" + accuracy_DL);
        System.out.println("DL Min Accuracy:" + min_DL_Acc);
        System.out.println("DL Max Accuracy:" + max_DL_Acc);

        repaint();
    }

    float min_SIMM_Acc = 100f;
    float min_DL_Acc = 100f;
    float max_SIMM_Acc = 0f;
    float max_DL_Acc = 0f;
    public void setDataIndex(int selectedIndex) {
        dataIndex = selectedIndex;

        String[] groundTruth = deepLearningTargetCoords[dataIndex].getLabels();
        String[] DIMM_Result = deepLearningSourceCoords[dataIndex].getLabels();
        String[] DL_Results = deepLearningResultCoords[dataIndex].getLabels();
        int coordsNum = groundTruth.length;
        int validSIMMCount = 0;
        int validDLCount = 0;
        for(int j = 0; j < groundTruth.length; j++) {
            if(groundTruth[j].equals(DIMM_Result[j])) {
                validSIMMCount += 1;
            }
            if(j < DL_Results.length && groundTruth[j].equals(DL_Results[j])) {
                validDLCount += 1;
            }
        }
        float accuracy_SIMM = (float) validSIMMCount / coordsNum * 100;
        float accuracy_DL = (float) validDLCount / coordsNum * 100;

        if(min_SIMM_Acc > accuracy_SIMM) min_SIMM_Acc = accuracy_SIMM;
        if(min_DL_Acc > accuracy_DL) min_DL_Acc = accuracy_DL;
        if(max_SIMM_Acc < accuracy_SIMM) max_SIMM_Acc = accuracy_SIMM;
        if(max_DL_Acc < accuracy_DL) max_DL_Acc = accuracy_DL;

        System.out.println("coords #:" + coordsNum);
        System.out.println("SIMM Accuracy #:" + accuracy_SIMM);
        System.out.println("DL Accuracy #:" + accuracy_DL);

        Map<String, Character> sDictionary = new HashMap<>();
        Character c = 0;
        for(String s : DIMM_Result) {
            if(!sDictionary.containsKey(s)) {
                sDictionary.put(s, c);
                c++;
            }
        }
        for(String s : DL_Results) {
            if(!sDictionary.containsKey(s)) {
                sDictionary.put(s, c);
                c++;
            }
        }
        for(String s : groundTruth) {
            if(!sDictionary.containsKey(s)) {
                sDictionary.put(s, c);
                c++;
            }
        }

        String linearDIMM = stringArrayToString(DIMM_Result, sDictionary);
        String linearDL = stringArrayToString(DL_Results, sDictionary);
        String linearGT = stringArrayToString(groundTruth, sDictionary);

        if(linearDL.length() > linearDIMM.length()) linearDL = linearDL.substring(0, linearDIMM.length());

        Levenshtein l = new Levenshtein();
        System.out.println("Levenshtein distance (DIMM, GT): " + l.distance(linearDIMM, linearGT));
        System.out.println("Levenshtein distance (DL, GT): " + l.distance(linearDL, linearGT));
        for(int i = 2; i < 4; i++) {
            NGram nGram = new NGram(i);
            System.out.println(i + "-gram distance (DIMM, GT): " + nGram.distance(linearDIMM, linearGT));
            System.out.println(i + "-gram distance (DL, GT): " + nGram.distance(linearDL, linearGT));

            QGram qGram = new QGram(i);
            System.out.println(i + "-qgram distance (DIMM, GT): " + qGram.distance(linearDIMM, linearGT));
            System.out.println(i + "-qgram distance (DL, GT): " + qGram.distance(linearDL, linearGT));

            System.out.println(i + " words seek distance (DIMM, GT): " + distanceNWords(DIMM_Result, groundTruth, i));
            System.out.println(i + " words seek distance (DL, GT): " + distanceNWords(DL_Results, groundTruth, i));
        }
        repaint();
    }

    private String stringArrayToString(String[] strings, Map<String,Character> dictionary) {
        StringBuilder sb = new StringBuilder();

        for(String string : strings) {
            Character c = dictionary.get(string);
            sb.append(c);
        }

        return sb.toString();
    }

    private double distanceNWords(String[] s1, String[] s2, final int N) {
        double count = 0;
        int MIN = Math.min(s1.length, s2.length);
        for(int j = 0; j < MIN; j++) {
            for(int i = j; i < j + (N - 1); i++) {
                if(i >= MIN) break;
                if(s1[j].equals(s2[i])) {
                    count += 1;
                    break;
                }
            }
        }

        return count / MIN;
    }

    public void setRawTrajectoryVisibility(boolean selected) {
        rawTrajectoryVisibility = selected;
        repaint();
    }

    public void setNoiseTrajectoryVisibility(boolean selected) {
        noiseTrajectoryVisibility = selected;
        repaint();
    }

    public void setGroundTruthGridVisibility(boolean selected) {
        groundTruthGridVisibility = selected;
        repaint();
    }

    public void setSourceDataGridVisibility(boolean selected) {
        sourceDataGridVisibility = selected;
        repaint();
    }

    public void setDeepLearningDataGridVisibility(boolean selected) {
        deepLearningDataGridVisibility = selected;
        repaint();
    }
}
