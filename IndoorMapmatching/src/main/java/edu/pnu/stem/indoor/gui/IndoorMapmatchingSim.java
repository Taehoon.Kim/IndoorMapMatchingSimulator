package edu.pnu.stem.indoor.gui;

import edu.pnu.stem.indoor.feature.IndoorFeatures;
import edu.pnu.stem.indoor.util.mapmatching.DirectIndoorMapMatching;
import edu.pnu.stem.indoor.util.movingobject.synthetic.SyntheticDataElement;
import edu.pnu.stem.indoor.util.parser.ChangeCoord;
import edu.pnu.stem.indoor.util.IndoorUtils;
import edu.pnu.stem.indoor.util.parser.DataUtils;
import edu.pnu.stem.indoor.util.parser.Trajectory;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.locationtech.jts.geom.LineString;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by STEM_KTH on 2017-05-17.
 * @author Taehoon Kim, Pusan National University, STEM Lab.
 */
public class IndoorMapmatchingSim {
    public final static int CANVAS_RIGHT_LOWER_X = 1000;
    public final static int CANVAS_RIGHT_LOWER_Y = 1000;
    public final static int CANVAS_LEFT_UPPER_X = 0;
    public final static int CANVAS_LEFT_UPPER_Y = 0;

    private JPanel panelMain;
    private JPanel panelCanvas;
    private JScrollPane jScrollPane;
    private JButton buttonCreateCell;
    private JButton buttonCreatePath;
    private JButton buttonSelectCell;
    private JButton buttonEvaluate;
    private JButton buttonSynthetic;
    private JButton buttonCreateHole;
    private JButton buttonGetTR_nextOne;
    private JButton buttonGetOSMData;
    private JButton buttonGetTR_prevOne;
    private JButton buttonGetBuildNGO;
    private JTextPane textPaneOriginal;
    private JButton buttonGetGridResults;
    private JCheckBox rawTrajectoryCheckBox;
    private JCheckBox noiseTrajectoryCheckBox;
    private JCheckBox groundTruthCheckBox;
    private JCheckBox deepLearningResultCheckBox;
    private JCheckBox sourceDataCheckBox;
    private JComboBox<Integer> comboBoxDataIndex;
    private JButton buttonGetIFC;
    private JButton buttonGetVitaData;

    private final String ROOT_PATH = System.getProperty("user.dir") + File.separator + "IndoorMapmatching" + File.separator + "res";
    private final String TR_PATH = ROOT_PATH + File.separator + "Real_positioningTrajectory";
    private final String GT_PATH = ROOT_PATH + File.separator + "Real_groundTruth" + File.separator + "gt_Trajectory_";
    private final String DL_PATH = ROOT_PATH + File.separator + "DeepLearning_Real_SFC";
    //private final String VITA_PATH = ROOT_PATH + File.separator + "Vita_210224";
    //private final String S_TR_PATH = VITA_PATH + File.separator + "indoor positioning data" + File.separator + "2021_02_24_15_51_18";
    //private final String S_GT_PATH = VITA_PATH + File.separator + "raw trajectory" + File.separator + "2021_02_24_15_38_38" + File.separator + "Dest_Traj_";
    //private final String VITA_PATH = ROOT_PATH + File.separator + "Vita_210220";
    //private final String S_TR_PATH = VITA_PATH + File.separator + "indoor positioning data" + File.separator + "2021_02_21_12_40_23";
    //private final String S_GT_PATH = VITA_PATH + File.separator + "raw trajectory" + File.separator + "2021_02_21_12_24_58" + File.separator + "Dest_Traj_";
    private final String VITA_PATH = ROOT_PATH + File.separator + "Vita";
    private final String S_TR_PATH = VITA_PATH + File.separator + "indoor positioning data" + File.separator + "2020_04_28_05_20_28";
    private final String S_GT_PATH = VITA_PATH + File.separator + "raw trajectory" + File.separator + "2020_04_28_05_10_02" + File.separator + "Dest_Traj_";

    private Dimension area;
    private int trIndexView = 1;
    private LineString trajectory = null;
    private LineString trajectoryIF = null;
    private LineString trajectoryGT = null;

    private IndoorMapmatchingSim() {
        area = new Dimension(CANVAS_RIGHT_LOWER_X - CANVAS_LEFT_UPPER_X,CANVAS_RIGHT_LOWER_Y - CANVAS_LEFT_UPPER_Y);

        buttonCreateHole.addActionListener(e -> ((CanvasPanel)panelCanvas).currentEditStatus = EditStatus.CREATE_HOLE);
        buttonCreateCell.addActionListener(e -> ((CanvasPanel)panelCanvas).currentEditStatus = EditStatus.CREATE_CELLSPACE);
        buttonCreatePath.addActionListener(e -> ((CanvasPanel)panelCanvas).currentEditStatus = EditStatus.CREATE_TRAJECTORY);
        buttonSelectCell.addActionListener(e -> {
            //((CanvasPanel)panelCanvas).currentEditStatus = EditStatus.SELECT_CELLSPACE);

            long startTime = System.currentTimeMillis();
            File dir = new File(S_TR_PATH);
            File[] fileList = dir.listFiles();
            assert fileList != null;

            int SAMPLE_NUMBER = 1000;
            if(SAMPLE_NUMBER > fileList.length) SAMPLE_NUMBER = fileList.length;
            ExperimentResult[] experimentResults = new ExperimentResult[SAMPLE_NUMBER];
            ArrayList<String> keyList = new ArrayList<>();
            for(int i = 0 ; i < SAMPLE_NUMBER ; i++){
                String fileID = "";
                File file = fileList[i];
                String[] stringGT = null;

                if(file.isFile()){
                    System.out.println("\tFile Name = " + file.getName());
                    String[] fileName = file.getName().split("_");
                    fileID = fileName[fileName.length - 1].split(Pattern.quote("."))[0];
                    File groundTruth = new File(S_GT_PATH + fileID + ".txt");

                    try {
                        // 1. get a trajectory data
                        LineString trajectory = DataUtils.getVITAData(file);

                        // 2. get a ground-truth data
                        LineString gt_trajectory = DataUtils.getVITAData(groundTruth);
                        DirectIndoorMapMatching dimm = new DirectIndoorMapMatching(((CanvasPanel)panelCanvas).getIndoorFeatures());
                        stringGT = dimm.getMapMatchingResult(gt_trajectory);
                        // *Exception
                        if(trajectory.isEmpty() || stringGT.length < trajectory.getNumPoints()) {
                            System.out.println("Pass");
                            experimentResults[i] = null;
                            continue;
                        }

                        // ground truth validation
                        IndoorFeatures indoorFeatures = ((CanvasPanel)panelCanvas).getIndoorFeatures();
                        boolean isValid = IndoorUtils.isValidByIndoorDistance(gt_trajectory, 5 * ChangeCoord.CANVAS_MULTIPLE, indoorFeatures);
                        /*
                        boolean[][] topoGraph = indoorFeatures.getTopologyGraph();
                        for (int j = 0; j < stringGT.length - 2; j++) {
                            int startIndex = indoorFeatures.getCellSpaceIndex(stringGT[j]);
                            int endIndex = indoorFeatures.getCellSpaceIndex(stringGT[j + 1]);
                            if(!topoGraph[startIndex][endIndex]) {
                                System.out.println("Ground truth is invalid");
                                isValid = false;
                                break;
                            }
                        }
                         */
                        if(!isValid) {
                            System.out.println("invalid");
                            continue;
                        }
                        double MAX_DISTANCE = ChangeCoord.CANVAS_MULTIPLE * 5;
                        LineString lineWithMaxIndoorDistance = IndoorUtils.applyIndoorDistanceFilter(trajectory, MAX_DISTANCE, indoorFeatures);
                        ((CanvasPanel)panelCanvas).setTrajectory_IF(lineWithMaxIndoorDistance);
                        ((CanvasPanel)panelCanvas).setTrajectory(trajectory);
                        ((CanvasPanel)panelCanvas).setTrajectory_GT(gt_trajectory);
                        ((CanvasPanel)panelCanvas).saveImage(fileID);

                        experimentResults[i] = ((CanvasPanel)panelCanvas).evaluateSIMM_Excel(fileID, keyList, stringGT);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        long endTime = System.currentTimeMillis();
                        System.out.println("Running Time :" + (endTime - startTime)/1000.0 + "sec");
                    }
                }
            }
            // 최종 결과 출력
            Workbook workbook = new XSSFWorkbook();
            Sheet summarySheet = workbook.createSheet("Summary");
            Row row = summarySheet.createRow(0);
            if(experimentResults[0] != null) {
                row.createCell(row.getLastCellNum()  + 1).setCellValue("ID");
                for(String key : keyList) {
                    row.createCell(row.getLastCellNum()).setCellValue("Accuracy: \n" + key);
                }
                for(String key : keyList) {
                    row.createCell(row.getLastCellNum()).setCellValue("TRUE count: \n" + key);
                }
                row.createCell(row.getLastCellNum()).setCellValue("Point count");
                row.createCell(row.getLastCellNum()).setCellValue("Trajectory Length\n Original");
                row.createCell(row.getLastCellNum()).setCellValue("Trajectory Length\n Indoor Filtered");
                row.createCell(row.getLastCellNum()).setCellValue("Trajectory Length\n Ground Truth");
                row.createCell(row.getLastCellNum()).setCellValue("Average Error\n Original");
                row.createCell(row.getLastCellNum()).setCellValue("Average Error\n Indoor Filtered");
                row.createCell(row.getLastCellNum()).setCellValue("Variance Error\n Original");
                row.createCell(row.getLastCellNum()).setCellValue("Variance Error\n Indoor Filtered");
            }

            for(ExperimentResult er : experimentResults) {
                if(er != null) {
                    row = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
                    row.createCell(row.getLastCellNum()  + 1).setCellValue(er.id);
                    for(String key : keyList) {
                        if(er.accuracy.containsKey(key)) {
                            row.createCell(row.getLastCellNum()).setCellValue(er.accuracy.get(key));
                        }
                        else {
                            row.createCell(row.getLastCellNum()).setCellValue("");
                        }
                    }
                    for(String key : keyList) {
                        if(er.trueCount.containsKey(key)) {
                            row.createCell(row.getLastCellNum()).setCellValue(er.trueCount.get(key));
                        }
                        else {
                            row.createCell(row.getLastCellNum()).setCellValue("");
                        }
                    }
                    row.createCell(row.getLastCellNum()).setCellValue(er.numTrajectoryPoint);
                    row.createCell(row.getLastCellNum()).setCellValue(er.trajectoryLength[0]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.trajectoryLength[1]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.trajectoryLength[2]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.averageError[0]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.averageError[1]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.varianceError[0]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.varianceError[1]);
                }
            }

            // Write all result in a excel file
            FileOutputStream outFile;
            try {
                outFile = new FileOutputStream("Result_Summary.xlsx");
                workbook.write(outFile);
                outFile.close();
                System.out.println("Experiments is End!!");
            } catch (Exception e1) {
                e1.printStackTrace();
                long endTime = System.currentTimeMillis();
                System.out.println("Running Time :" + (endTime - startTime)/1000.0 + "sec");
            }
            long endTime = System.currentTimeMillis();
            System.out.println("Running Time :" + (endTime - startTime)/1000.0 + "sec");
        });
        buttonGetGridResults.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser(DL_PATH);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("txt (*.txt)", "txt");
            fileChooser.setFileFilter(filter);

            int returnVal = fileChooser.showOpenDialog(panelMain);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                File sourceGridDataFile = new File(DL_PATH + File.separator + "src-test.txt");
                File targetGridDataFile = new File(DL_PATH + File.separator + "tgt-test.txt");
                File rawTrajectoryFile  = new File(DL_PATH + File.separator + "raw_trajectory.txt");
                File noiseTrajectoryFile = new File(DL_PATH + File.separator + "noise_trajectory.txt");
                try {
                    Trajectory[] deepLearningGridResult = DataUtils.getDeepLearningResults(file);
                    Trajectory[] sourceGridResult = DataUtils.getDeepLearningResults(sourceGridDataFile);
                    Trajectory[] targetGridResult = DataUtils.getDeepLearningResults(targetGridDataFile);
                    LineString[] rawTrajectories = DataUtils.getTrajectory(rawTrajectoryFile);
                    LineString[] noiseTrajectories = DataUtils.getTrajectory(noiseTrajectoryFile);

                    assert deepLearningGridResult != null;
                    assert sourceGridResult != null;
                    assert targetGridResult != null;
                    ((CanvasPanel)panelCanvas).setDeepLearningResults(deepLearningGridResult, sourceGridResult, targetGridResult, rawTrajectories, noiseTrajectories);

                    for(int i = 0; i < deepLearningGridResult.length; i++)
                        comboBoxDataIndex.addItem(i);
                    ((CanvasPanel)panelCanvas).setRawTrajectoryVisibility(rawTrajectoryCheckBox.isSelected());
                    ((CanvasPanel)panelCanvas).setNoiseTrajectoryVisibility(noiseTrajectoryCheckBox.isSelected());
                    ((CanvasPanel)panelCanvas).setGroundTruthGridVisibility(groundTruthCheckBox.isSelected());
                    ((CanvasPanel)panelCanvas).setSourceDataGridVisibility(sourceDataCheckBox.isSelected());
                    ((CanvasPanel)panelCanvas).setDeepLearningDataGridVisibility(deepLearningResultCheckBox.isSelected());

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            else {
                System.out.println("Cancel");
            }
        });
        buttonGetOSMData.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser(ROOT_PATH + File.separator + "OSM");
            FileNameExtensionFilter filter = new FileNameExtensionFilter("OSM Data (*.osm)", "osm");
            fileChooser.setFileFilter(filter);

            int returnVal = fileChooser.showOpenDialog(panelMain);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                try {
                    IndoorFeatures indoorFeatures = DataUtils.getIndoorFeaturesFromOSMXML(file);
                    ((CanvasPanel)panelCanvas).setIndoorFeatures(indoorFeatures);
                } catch (IOException | ParserConfigurationException | SAXException e1) {
                    e1.printStackTrace();
                }
            }
            else {
                System.out.println("Cancel");
            }
        });
        buttonGetBuildNGO.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser(TR_PATH);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("txt (*.txt)", "txt");
            fileChooser.setFileFilter(filter);

            String fileID = "";
            int returnVal = fileChooser.showOpenDialog(panelMain);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                try {
                    System.out.println("File Name: " + file.getName());
                    String[] fileName = file.getName().split("_");
                    fileID = fileName[fileName.length - 1];

                    LineString trajectory = DataUtils.getBuildNGoData(file);
                    if(trajectory.isEmpty()) return;
                    ((CanvasPanel)panelCanvas).setTrajectory(trajectory);

                    // For making input data (for deep learning)
                    FileWriter fw = new FileWriter(DL_PATH + File.separator + "src-test-"+ fileID);
                    PrintWriter pw = new PrintWriter(fw);

                    DirectIndoorMapMatching dimm = new DirectIndoorMapMatching(((CanvasPanel)panelCanvas).getIndoorFeatures());
                    String[] dimm_label_List = dimm.getMapMatchingResult(trajectory);
                    for(int i = 0; i < trajectory.getNumPoints(); i++){
                        String grid_coord = SyntheticDataElement.getGridID(trajectory.getCoordinateN(i));
                        pw.print(dimm_label_List[i] + "@" + grid_coord + " ");
                    }
                    pw.println(".");
                    pw.close();
                    // Making End!

                    LineString lineWithMaxIndoorDistance = IndoorUtils.applyIndoorDistanceFilter(trajectory, ChangeCoord.CANVAS_MULTIPLE * 4, ((CanvasPanel)panelCanvas).getIndoorFeatures());
                    ((CanvasPanel)panelCanvas).setTrajectory_IF(lineWithMaxIndoorDistance);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                String[] gt = null;
                File groundTruth = new File(GT_PATH + fileID);
                try {
                    gt = DataUtils.getBuildNGoGroundTruth(groundTruth);

                    // For making target data
                    FileWriter fw = new FileWriter(DL_PATH + File.separator + "tgt-test-" + fileID);
                    PrintWriter pw = new PrintWriter(fw);

                    assert gt != null;
                    for (String s : gt) {
                        pw.print(s + "@" + " ");
                    }
                    pw.println(".");
                    pw.close();
                    // Making End!

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                ((CanvasPanel)panelCanvas).doIndoorMapMatching(textPaneOriginal);
                ((CanvasPanel)panelCanvas).getGroundTruthResult(textPaneOriginal, gt);
            }
            else {
                System.out.println("Cancel");
            }
        });
        buttonGetTR_nextOne.addActionListener(e -> {
            if(trajectory != null && trajectoryIF != null) {
                if(trajectory.getNumPoints() > trIndexView && trIndexView > 0) {
                    trIndexView++;
                    System.out.println("=======================");
                    System.out.println("Index: " + trIndexView);
                    IndoorUtils.indoorDistance(trajectoryIF.getCoordinateN(trIndexView -1), trajectory.getCoordinateN(trIndexView),5*ChangeCoord.CANVAS_MULTIPLE,((CanvasPanel)panelCanvas).getIndoorFeatures());
                    ((CanvasPanel)panelCanvas).setTrajectory((LineString) CanvasPanel.getSubLineString(trajectory, trIndexView));
                    ((CanvasPanel)panelCanvas).setTrajectory_IF((LineString) CanvasPanel.getSubLineString(trajectoryIF, trIndexView));
                }
                else {
                    trajectory = trajectoryIF = trajectoryGT = null;
                    trIndexView = 1;
                }
            }
            else {
                trajectory = ((CanvasPanel)panelCanvas).getTrajectory();
                trajectoryIF = ((CanvasPanel)panelCanvas).getTrajectory_IF();
            }
        });
        buttonGetTR_prevOne.addActionListener(e -> {
            if(trajectory != null && trajectoryIF != null) {
                if(trajectory.getNumPoints() > trIndexView && trIndexView > 2) {
                    trIndexView--;

                    ((CanvasPanel)panelCanvas).setTrajectory((LineString) CanvasPanel.getSubLineString(trajectory, trIndexView));
                    ((CanvasPanel)panelCanvas).setTrajectory_IF((LineString) CanvasPanel.getSubLineString(trajectoryIF, trIndexView));
                }
                else {
                    trIndexView = 3;
                }
            }
            else {
                trajectory = ((CanvasPanel)panelCanvas).getTrajectory();
                trajectoryIF = ((CanvasPanel)panelCanvas).getTrajectory_IF();
            }
        });
        buttonEvaluate.addActionListener(e -> {
            long startTime = System.currentTimeMillis();
            //File dir = new File(S_TR_PATH);
            File dir = new File(TR_PATH);
            File[] fileList = dir.listFiles();
            assert fileList != null;

            int SAMPLE_NUMBER = 1000;
            if(SAMPLE_NUMBER > fileList.length) SAMPLE_NUMBER = fileList.length;
            ExperimentResult[] experimentResults = new ExperimentResult[SAMPLE_NUMBER];
            ArrayList<String> keyList = new ArrayList<>();
            for(int i = 0 ; i < SAMPLE_NUMBER ; i++){
                String fileID = "";
                File file = fileList[i];
                String[] stringGT = null;

                if(file.isFile()){
                    System.out.println("\tFile Name = " + file.getName());
                    String[] fileName = file.getName().split("_");
                    fileID = fileName[fileName.length - 1].split(Pattern.quote("."))[0];
                    //File groundTruth = new File(S_GT_PATH + fileID + ".txt");
                    File groundTruth = new File(GT_PATH + fileID + ".txt");

                    try {
                        // 1. get a trajectory data
                        //LineString trajectory = DataUtils.getVITAData(file);
                        LineString trajectory = DataUtils.getBuildNGoData(file);
                        // 2. get a ground-truth data
//                        LineString gt_trajectory = DataUtils.getVITAData(groundTruth);
//                        DirectIndoorMapMatching dimm = new DirectIndoorMapMatching(((CanvasPanel)panelCanvas).getIndoorFeatures());
//                        stringGT = dimm.getMapMatchingResult(gt_trajectory);
                        stringGT = DataUtils.getBuildNGoGroundTruth(groundTruth);
                        // *Exception
                        if(trajectory.isEmpty() || stringGT.length < trajectory.getNumPoints()) {
                            System.out.println("Pass");
                            experimentResults[i] = null;
                            continue;
                        }
                        ((CanvasPanel)panelCanvas).setTrajectory(trajectory);
                        //((CanvasPanel)panelCanvas).setTrajectory_GT(gt_trajectory);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        long endTime = System.currentTimeMillis();
                        System.out.println("Running Time :" + (endTime - startTime)/1000.0 + "sec");
                    }
                }
                experimentResults[i] = ((CanvasPanel)panelCanvas).evaluateSIMM_Excel(fileID, keyList, stringGT);
                //experimentResults[i] = ((CanvasPanel)panelCanvas).evaluateSIMM_forSIG(fileID, keyList, stringGT);
                ((CanvasPanel)panelCanvas).saveImage(fileID);
            }
            // 최종 결과 출력
            Workbook workbook = new XSSFWorkbook();
            Sheet summarySheet = workbook.createSheet("Summary");
            Row row = summarySheet.createRow(0);
            if(experimentResults[0] != null) {
                row.createCell(row.getLastCellNum()  + 1).setCellValue("ID");
                for(String key : keyList) {
                    row.createCell(row.getLastCellNum()).setCellValue("Accuracy: \n" + key);
                }
                for(String key : keyList) {
                    row.createCell(row.getLastCellNum()).setCellValue("TRUE count: \n" + key);
                }
                row.createCell(row.getLastCellNum()).setCellValue("Point count");
                row.createCell(row.getLastCellNum()).setCellValue("Trajectory Length\n Original");
                row.createCell(row.getLastCellNum()).setCellValue("Trajectory Length\n Indoor Filtered");
            }

            for(ExperimentResult er : experimentResults) {
                if(er != null) {
                    row = summarySheet.createRow(summarySheet.getLastRowNum() + 1);
                    row.createCell(row.getLastCellNum()  + 1).setCellValue(er.id);
                    for(String key : keyList) {
                        if(er.accuracy.containsKey(key)) {
                            row.createCell(row.getLastCellNum()).setCellValue(er.accuracy.get(key));
                        }
                        else {
                            row.createCell(row.getLastCellNum()).setCellValue("");
                        }
                    }
                    for(String key : keyList) {
                        if(er.trueCount.containsKey(key)) {
                            row.createCell(row.getLastCellNum()).setCellValue(er.trueCount.get(key));
                        }
                        else {
                            row.createCell(row.getLastCellNum()).setCellValue("");
                        }
                    }
                    row.createCell(row.getLastCellNum()).setCellValue(er.numTrajectoryPoint);
                    row.createCell(row.getLastCellNum()).setCellValue(er.trajectoryLength[0]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.trajectoryLength[1]);
                    row.createCell(row.getLastCellNum()).setCellValue(er.trajectoryLength[2]);
                }
            }

            // Write all result in a excel file
            FileOutputStream outFile;
            try {
                outFile = new FileOutputStream("Result_Summary.xlsx");
                workbook.write(outFile);
                outFile.close();
                System.out.println("Experiments is End!!");
            } catch (Exception e1) {
                e1.printStackTrace();
                long endTime = System.currentTimeMillis();
                System.out.println("Running Time :" + (endTime - startTime)/1000.0 + "sec");
            }
            long endTime = System.currentTimeMillis();
            System.out.println("Running Time :" + (endTime - startTime)/1000.0 + "sec");
        });
        buttonSynthetic.addActionListener(e ->
                ((CanvasPanel)panelCanvas).syntheticTrajectoryTest(textPaneOriginal));
        comboBoxDataIndex.addActionListener(e ->
                ((CanvasPanel)panelCanvas).setDataIndex(comboBoxDataIndex.getSelectedIndex()));
        rawTrajectoryCheckBox.addActionListener(e ->
                ((CanvasPanel)panelCanvas).setRawTrajectoryVisibility(rawTrajectoryCheckBox.isSelected()));
        noiseTrajectoryCheckBox.addActionListener(e ->
                ((CanvasPanel)panelCanvas).setNoiseTrajectoryVisibility(noiseTrajectoryCheckBox.isSelected()));
        groundTruthCheckBox.addActionListener(e ->
                ((CanvasPanel)panelCanvas).setGroundTruthGridVisibility(groundTruthCheckBox.isSelected()));
        sourceDataCheckBox.addActionListener(e ->
                ((CanvasPanel)panelCanvas).setSourceDataGridVisibility(sourceDataCheckBox.isSelected()));
        deepLearningResultCheckBox.addActionListener(e ->
                ((CanvasPanel)panelCanvas).setDeepLearningDataGridVisibility(deepLearningResultCheckBox.isSelected()));
        buttonGetIFC.addActionListener(e ->
                ((CanvasPanel)panelCanvas).setIndoorFeatures(DataUtils.getIndoorInfoWithIFCFormat()));
        buttonGetVitaData.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser(S_TR_PATH);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("txt (*.txt)", "txt");
            fileChooser.setFileFilter(filter);

            String fileID = "";
            int returnVal = fileChooser.showOpenDialog(panelMain);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                try {
                    System.out.println("File Name: " + file.getName());
                    String[] fileName = file.getName().split("_");
                    fileID = fileName[fileName.length - 1];

                    LineString trajectory = DataUtils.getVITAData(file);
                    if(trajectory.isEmpty()) return;
                    ((CanvasPanel)panelCanvas).setTrajectory(trajectory);

                    LineString lineWithMaxIndoorDistance = IndoorUtils.applyIndoorDistanceFilter(
                            trajectory, ChangeCoord.CANVAS_MULTIPLE * 4, ((CanvasPanel)panelCanvas).getIndoorFeatures());
                    ((CanvasPanel)panelCanvas).setTrajectory_IF(lineWithMaxIndoorDistance);
                } catch (IOException | ParseException ioException) {
                    ioException.printStackTrace();
                }
            }

            String[] gt = null;
            File groundTruth = new File(S_GT_PATH + fileID);
            try {
                LineString gt_trajectory = DataUtils.getVITAData(groundTruth);
                IndoorFeatures indoorFeatures = ((CanvasPanel)panelCanvas).getIndoorFeatures();
                IndoorUtils.isValidByIndoorDistance(gt_trajectory, 5 * ChangeCoord.CANVAS_MULTIPLE, indoorFeatures);

                ((CanvasPanel)panelCanvas).setTrajectory_GT(gt_trajectory);
                DirectIndoorMapMatching dimm = new DirectIndoorMapMatching(((CanvasPanel)panelCanvas).getIndoorFeatures());
                gt = dimm.getMapMatchingResult(gt_trajectory);
            } catch (IOException | ParseException ioException) {
                ioException.printStackTrace();
            }

            ((CanvasPanel)panelCanvas).doIndoorMapMatching(textPaneOriginal);
            ((CanvasPanel)panelCanvas).getGroundTruthResult(textPaneOriginal, gt);
        });
    }

    public static void main(String[] args) {
        IndoorMapmatchingSim indoorMapmatchingSim = new IndoorMapmatchingSim();
        JFrame jFrame = new JFrame("Symbolic Indoor Map Matching Simulator");
        jFrame.setContentPane(indoorMapmatchingSim.panelMain);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setVisible(true);

        /*
        double lat = 35.627701;
        double lon = 139.757083;
        Coordinate start = new Coordinate(lat, lon);

        double xMeterUnit = 0.1;
        double yMeterUnit = 0.1;

        double distance = ChangeCoord.HaversineInM(start, new Coordinate(lat, lon + xMeterUnit));
        while(distance > 1.0 ) {
            xMeterUnit *= 0.1;
            distance = ChangeCoord.HaversineInM(start, new Coordinate(lat, lon + xMeterUnit));
        }
        double tmpXMeterUnit = xMeterUnit;
        while(distance < 1.0 ) {
            tmpXMeterUnit += xMeterUnit*0.1;
            distance = ChangeCoord.HaversineInM(start, new Coordinate(lat, lon + tmpXMeterUnit));
        }
        System.out.println("distance: " + distance + " // X_meter_unit: " + tmpXMeterUnit);

        distance = ChangeCoord.HaversineInM(start, new Coordinate(lat + yMeterUnit, lon));
        while(distance > 1.0 ) {
            yMeterUnit *= 0.1;
            distance = ChangeCoord.HaversineInM(start, new Coordinate(lat + yMeterUnit, lon));
        }
        double tmpYMeterUnit = yMeterUnit;
        if(distance < 0.5) {
            tmpYMeterUnit = yMeterUnit * 10;
            distance = ChangeCoord.HaversineInM(start, new Coordinate(lat + tmpYMeterUnit, lon));
            while(distance > 1.0 ) {
                tmpYMeterUnit -= yMeterUnit*0.1;
                distance = ChangeCoord.HaversineInM(start, new Coordinate(lat + tmpYMeterUnit, lon));
            }
        }
        else {
            while(distance < 1.0 ) {
                tmpYMeterUnit += yMeterUnit*0.1;
                distance = ChangeCoord.HaversineInM(start, new Coordinate(lat + tmpYMeterUnit, lon));
            }
        }
        System.out.println("distance: " + distance + " // Y_meter_unit: " + tmpYMeterUnit);
        */
    }

    private void createUIComponents() {
        // TODO: Place custom component creation code here
        panelCanvas = new CanvasPanel(this);
        jScrollPane = new JScrollPane(panelCanvas);
    }

    public void changeCanvasArea(Dimension area) {
        if(!panelCanvas.getPreferredSize().equals(area)){
            panelCanvas.setPreferredSize(area);
            panelCanvas.revalidate();
        }
    }
}
