package edu.pnu.stem.indoor.util.movingobject.synthetic;

import edu.pnu.stem.indoor.util.parser.DataUtils;
import org.davidmoten.hilbert.HilbertCurve;
import org.davidmoten.hilbert.SmallHilbertCurve;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;

import java.util.ArrayList;

public class SyntheticDataElement {
    private LineString rawTrajectory;
    private ArrayList<LineString> noiseTrajectories;

    private String[] groundTruth_SIMM;
    private ArrayList<String[]> noise_SIMM;

    private String[] groundTruthTrajectory_gridLayout;
    private ArrayList<String[]> noiseTrajectories_gridLayout;

    SyntheticDataElement(LineString rawTrajectory, ArrayList<LineString> noiseTrajectories, String[] groundTruth_SIMM, ArrayList<String[]> noise_SIMM){
        this.rawTrajectory = rawTrajectory;
        this.noiseTrajectories = noiseTrajectories;
        this.groundTruth_SIMM = groundTruth_SIMM;
        this.noise_SIMM = noise_SIMM;

        groundTruthTrajectory_gridLayout = new String[rawTrajectory.getNumPoints()];
        for(int i = 0; i < rawTrajectory.getNumPoints(); i++) {
            groundTruthTrajectory_gridLayout[i] = getGridID(rawTrajectory.getCoordinateN(i));
        }

        noiseTrajectories_gridLayout = new ArrayList<>();
        for (LineString noiseTrajectory : noiseTrajectories) {
            String[] noise_SIMM_result = new String[noiseTrajectory.getNumPoints()];
            for (int i = 0; i < noiseTrajectory.getNumPoints(); i++) {
                noise_SIMM_result[i] = getGridID(noiseTrajectory.getCoordinateN(i));
            }
            noiseTrajectories_gridLayout.add(noise_SIMM_result.clone());
        }
    }

    public static String getGridID(Coordinate coordinateN) {
        int int_x = (int) (coordinateN.getX() / DataUtils.GRID_SIZE);
        int int_y = (int) (coordinateN.getY() / DataUtils.GRID_SIZE);

        // Making SFC code
        return String.valueOf(DataUtils.hilbertCurve.index(int_x, int_y));
        /*
        String x_id = String.valueOf(int_x);
        String y_id = String.valueOf(int_y);

        return x_id + "_" + y_id;
         */
    }

    public LineString getRawTrajectory() {
        return rawTrajectory;
    }

    public ArrayList<LineString> getNoiseTrajectories() {
        return noiseTrajectories;
    }

    public String[] getGroundTruth_SIMM() {
        return groundTruth_SIMM;
    }

    public ArrayList<String[]> getNoise_SIMM() {
        return noise_SIMM;
    }

    public String[] getGroundTruthTrajectory_gridLayout() {
        return groundTruthTrajectory_gridLayout;
    }

    public ArrayList<String[]> getNoiseTrajectories_gridLayout() {
        return noiseTrajectories_gridLayout;
    }
}
