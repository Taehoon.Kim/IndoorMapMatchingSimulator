package edu.pnu.stem.indoor.util;

import org.locationtech.jts.geom.LineString;

public class IndoorRoute {
    public LineString route;
    public int lastCellIndex;

    public IndoorRoute(LineString resultIndoorPath, int lastCellIndex) {
        this.route = resultIndoorPath;
        this.lastCellIndex = lastCellIndex;
    }
}
