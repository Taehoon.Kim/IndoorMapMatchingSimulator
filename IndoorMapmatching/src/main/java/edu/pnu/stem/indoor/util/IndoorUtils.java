package edu.pnu.stem.indoor.util;


import edu.pnu.stem.indoor.feature.CellSpace;
import edu.pnu.stem.indoor.feature.IndoorFeatures;
import edu.pnu.stem.indoor.feature.VisibilityGraph;
import edu.pnu.stem.indoor.util.parser.ChangeCoord;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.operation.distance.DistanceOp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by STEM_KTH on 2017-06-09.
 * @author Taehoon Kim, Pusan National University, STEM Lab.
 */
public class IndoorUtils {
    public static final int BUFFER_SIZE = (int) ChangeCoord.CANVAS_MULTIPLE;
    public static final int EPSILON = 5;
    private static final GeometryFactory gf = new GeometryFactory();

    /**
     * This function provides an indoor route that consider the indoor space geometry for a given trajectory.
     *
     * @param trajectory It consists of two points(start and end point) that want to create an indoor route
     * @param cellSpaces List of CellSpace that contains all indoor space information in the building
     * @return Indoor route
     * */
    public static IndoorRoute getIndoorRoute(LineString trajectory, ArrayList<CellSpace> cellSpaces) {
        // TODO : Make IndoorRoute with way-points (currently make with just two points)
        LineString resultIndoorPath = null;
        int startPCellIndex = -1;
        int endPCellIndex = -1;

        Point startP = trajectory.getStartPoint();
        Point endP = trajectory.getEndPoint();

        int cellIndex = 0;
        for (CellSpace cellSpace : cellSpaces) {
            Polygon cellSpaceGeom = cellSpace.getGeom();
            /*
            possible case : locate of trajectory coordinates
            1. contain in a cell
            2. on a cell boundary
            2-1. on a cell door boundary
            */
            if (cellSpaceGeom.covers(startP) && cellSpaceGeom.covers(endP)) {
                /*
                In case : trajectory is included in same cell
                but in this case, one of the trajectory boundary point possible to located at the boundary of the cell
                If it is located at the boundary of the cell,
                Need to check it is also located at the boundary of the door or not
                Because, If it isn't located at the boundary of the door,
                The two points must be distinguished as being located in another space.
                */
                boolean isStartPOnADoor = false;
                boolean isEndPOnADoor = false;
                for(LineString doorGeom : cellSpace.getDoors()) {
                    if(doorGeom.getStartPoint().equals(startP) || doorGeom.getEndPoint().equals(startP)) {
                        isStartPOnADoor = true;
                    }
                    if(doorGeom.getStartPoint().equals(endP) || doorGeom.getEndPoint().equals(endP)) {
                        isEndPOnADoor = true;
                    }
                }
                if(!cellSpaceGeom.contains(startP) && !isStartPOnADoor) {
                    endPCellIndex = cellIndex;
                }
                else if(!cellSpaceGeom.contains(endP) && !isEndPOnADoor) {
                    startPCellIndex = cellIndex;
                }
                else {
                    startPCellIndex = endPCellIndex = cellIndex;
                    resultIndoorPath = makeIndoorRouteInCell(startP, endP, cellSpace);
                    break;
                }
            }
            else {
                if(cellSpaceGeom.covers(startP) && startPCellIndex == -1) {
                    startPCellIndex = cellIndex;
                }
                else if(cellSpaceGeom.covers(endP) && endPCellIndex == -1) {
                    endPCellIndex = cellIndex;
                }
            }
            cellIndex++;
        }

        if(startPCellIndex == -1 || endPCellIndex == -1) {
            // For the Thick model, need to handle the case where coordinates are entered between walls
            if(startPCellIndex == -1) {
                startPCellIndex = getCellSpaceIndexWithEpsilon(startP.getCoordinate(), cellSpaces);
                startP = getNearestPoint(startP, cellSpaces.get(startPCellIndex).getGeom());
            }
            if(endPCellIndex == -1) {
                endPCellIndex = getCellSpaceIndexWithEpsilon(endP.getCoordinate(), cellSpaces);
                endP = getNearestPoint(endP, cellSpaces.get(endPCellIndex).getGeom());
            }

            if(startPCellIndex == -1 || endPCellIndex == -1) {
                // In case : trajectory is defined outside of the cells
                // TODO : Make alert the path of point is must include in Cell Space
                System.out.println("The coordinates were bounced off the building");
            }
            else if (startPCellIndex == endPCellIndex) {
                resultIndoorPath = makeIndoorRouteInCell(startP, endP, cellSpaces.get(startPCellIndex));
            }
            else {
                resultIndoorPath = getIndoorRoute(startPCellIndex, endPCellIndex, startP, endP, cellSpaces);
                if(resultIndoorPath == null) {
                    // In case : There is no connection between start and end
                    // In this case, the endP is corrected to belong to the cell to which startP belongs
                    // There are two ways to do this: Currently implemented in 2
                    // 1. Creates a straight line between startP and endP, corrects to a point intersecting CellBoundary
                    // 2. Corrected to the closest point between endP and CellBoundary
                    // Then find the indoor route in the same cell
                    endP = getNearestPoint(endP, cellSpaces.get(startPCellIndex).getGeom());
                    resultIndoorPath = makeIndoorRouteInCell(startP, endP, cellSpaces.get(startPCellIndex));
                }
            }
        }
        else if(resultIndoorPath == null) {
            resultIndoorPath = getIndoorRoute(startPCellIndex, endPCellIndex, startP, endP, cellSpaces);
        }

        return new IndoorRoute(resultIndoorPath, endPCellIndex);
    }

    /**
     * In case : trajectory cover several cell spaces
     * Consider only same floor's doors connection
     * TODO : Make door2door graph take into account several floors
     *
     * @param startPCellIndex
     * @param endPCellIndex
     * @param startP
     * @param endP
     * @param cellSpaces
     * @return
     */
    private static LineString getIndoorRoute(int startPCellIndex, int endPCellIndex, Point startP, Point endP, ArrayList<CellSpace> cellSpaces) {
        LineString resultIndoorPath = null;
        VisibilityGraph graph = new VisibilityGraph();

        if(startPCellIndex != endPCellIndex) {
            // Makes door2door graph
            if(VisibilityGraph.getBaseGraph() == null) {
                ArrayList<LineString> doors = new ArrayList<>();
                for (CellSpace cellSpace: cellSpaces) {
                    ArrayList<LineString> d2dGraph = cellSpace.getDoor2doorEdges();
                    if(d2dGraph != null && d2dGraph.size() != 0) {
                        graph.addEdges(d2dGraph);
                    }
                    doors.addAll(cellSpace.getDoors());
                }
                // Determine whether the indoor model is thin or thick by door objects
                // If it is a thick model, creates edges for the door2door graph by finding separated two objects but actually same object (door).
                HashSet<LineString> interDoorGraph = new HashSet<>();
                for(int i = 0; i < doors.size(); i++) {
                    for(int j = i + 1; j <  doors.size(); j++) {
                        LineString doorA = doors.get(i);
                        LineString doorB = doors.get(j);
                        if(doorA.equals(doorB)) {
                            break; // In case: thin model
                        }
                        else {
                            if(doorA.buffer(BUFFER_SIZE,2).covers(doorB)) { // In case: thick model
                                interDoorGraph.add(gf.createLineString(new Coordinate[]{doorA.getStartPoint().getCoordinate(), doorB.getStartPoint().getCoordinate()}));
                                interDoorGraph.add(gf.createLineString(new Coordinate[]{doorA.getEndPoint().getCoordinate(), doorB.getEndPoint().getCoordinate()}));
                            }
                        }
                    }
                }
                doors.clear();
                doors.addAll(interDoorGraph);
                graph.addEdges(doors);

                VisibilityGraph.setBaseGraph(graph.getEdges());
            }
            else {
                // Reuse VisibilityGraph object using base graph
                graph = VisibilityGraph.getBaseGraph();
            }
            // Make point2door edges and reflects it to door2door graph
            ArrayList<LineString> start2doorGraph =  makePoint2DoorEdge(startP, cellSpaces.get(startPCellIndex));
            ArrayList<LineString> end2doorGraph =  makePoint2DoorEdge(endP, cellSpaces.get(endPCellIndex));
            graph.addEdges(start2doorGraph);
            graph.addEdges(end2doorGraph);
            // Get point2point shortest path using door2door graph
            if(end2doorGraph.isEmpty()) {
                // In case: a cell that containing the end point is disconnected with a building
                // So, make a point that intersect point between a original line and a cell boundary that containing the start point
                LineString lineString = gf.createLineString(new Coordinate[]{startP.getCoordinate(), endP.getCoordinate()});
                Geometry intersectionP = lineString.intersection(cellSpaces.get(startPCellIndex).getGeom().getExteriorRing());
                Coordinate newEndPointCoord = intersectionP.getCoordinates()[0];
                resultIndoorPath = gf.createLineString(new Coordinate[]{startP.getCoordinate(), newEndPointCoord});
            }
            else {
                Coordinate[] coords = new Coordinate[]{startP.getCoordinate(), endP.getCoordinate()};
                resultIndoorPath = graph.getShortestRoute(coords);
            }
        }
        else {
            System.out.println("Impossible case: cell Index is same!");
        }

        return resultIndoorPath;
    }

    /**
     * A function that returns all paths from a given point to doors in a target cell spaces.
     *
     * @param point given point
     * @param targetCellSpace target cell space with doors
     * @return all path from point to doors
     * */
    private static ArrayList<LineString> makePoint2DoorEdge(Point point, CellSpace targetCellSpace) {
        ArrayList<LineString> p2dGraph = new ArrayList<>();
        ArrayList<LineString> doors = targetCellSpace.getDoors();
        for (LineString door: doors) {
            for(int i = 0; i < door.getNumPoints(); i++) {
                Point doorP = door.getPointN(i);

                // Exception Case : Ignore If the door's point and given point are the same
                if(!point.equals(doorP)) {
                    LineString point2SDoorPath = makeIndoorRouteInCell(point, doorP, targetCellSpace);
                    p2dGraph.add(point2SDoorPath);
                    p2dGraph.add((LineString) point2SDoorPath.reverse());
                }
            }
        }
        return p2dGraph;
    }

    /**
     * A function to obtain the path between start and end points.
     * This function assumes that the start and end points are in the same cell space.
     *
     * @param startP start point
     * @param endP end point
     * @param cellSpace cell space with geometric information
     * @return Indoor route between start point and end point
     * */
    private static LineString makeIndoorRouteInCell(Point startP, Point endP, CellSpace cellSpace) {
        LineString p2pIndoorPath;
        Polygon cellSpaceGeom = cellSpace.getGeom();


        if(!cellSpaceGeom.covers(startP)) {
            startP = getNearestPoint(startP, cellSpaceGeom);
        }
        else if(!cellSpaceGeom.covers(endP)) {
            endP = getNearestPoint(endP, cellSpaceGeom);
        }


        Coordinate[] coords = new Coordinate[]{startP.getCoordinate(), endP.getCoordinate()};
        LineString lineString = gf.createLineString(coords);

        // TODO: How to determine that topology relation between a linestring and cell geometry exterior??
        if(cellSpaceGeom.covers(lineString)) {  // in thick model, cover operation is better than contains
            // In case : The Cell contains a straight line between start and end points
            p2pIndoorPath = gf.createLineString(coords);
        }
        else {
            /*
            In case : The Cell doesn't contains a straight line between start and end points
            Make indoor path using the cell's visibility graph that added temp information(start and end points)
            */
            ArrayList<LineString> temporalGraph = cellSpace.addNodeToVGraph(startP.getCoordinate(), endP.getCoordinate());
            VisibilityGraph graph = new VisibilityGraph();
            graph.addEdges(temporalGraph);
            p2pIndoorPath = graph.getShortestRoute(coords);
        }

        return p2pIndoorPath;
    }

    private final static double tolerance = 0.001;
    /**
     *
     *
     * @param point
     * @param polygon
     * @return
     */
    private static Point getNearestPoint(Point point, Polygon polygon) {
        Coordinate[] coordinates = DistanceOp.nearestPoints(polygon, point);
        point = gf.createPoint(coordinates[0]);

        // If floating point error is occurred
        if(!polygon.covers(point)) {
            Point[] points = new Point[4];
            points[0] = gf.createPoint(new Coordinate(point.getX() - tolerance, point.getY() - tolerance));
            points[1] = gf.createPoint(new Coordinate(point.getX() + tolerance, point.getY() - tolerance));
            points[2] = gf.createPoint(new Coordinate(point.getX() - tolerance, point.getY() + tolerance));
            points[3] = gf.createPoint(new Coordinate(point.getX() + tolerance, point.getY() + tolerance));
            for(int i = 0; i < 4; i++) {
                if(polygon.covers(points[i])) {
                    point = points[i];
                    break;
                }
            }
        }

        return point;
    }

    /**
     *
     * @param targetCoordinate
     * @param cellSpaces
     * @return
     * */
    public static Integer getCellSpaceIndexWithEpsilon(Coordinate targetCoordinate, ArrayList<CellSpace> cellSpaces) {
        int epsilon = EPSILON;
        int cellIndex = getCellSpaceIndexWithEpsilon(targetCoordinate, epsilon, cellSpaces);
        while (cellIndex == -1) {
            epsilon *= 2;
            cellIndex = getCellSpaceIndexWithEpsilon(targetCoordinate, epsilon, cellSpaces);
        }

        return cellIndex;
    }

    /**
     *
     * @param targetCoordinate
     * @param epsilon
     * @param cellSpaces
     * @return
     * */
    public static Integer getCellSpaceIndexWithEpsilon(Coordinate targetCoordinate, double epsilon, ArrayList<CellSpace> cellSpaces) {
        Point point = gf.createPoint(targetCoordinate);
        Polygon bufferedPolygon = (Polygon) point.buffer(epsilon, 2);

        int closestCellIndex = -1;
        HashMap<Integer, Double> resultWithArea = new HashMap<>();
        for (CellSpace cellSpace : cellSpaces) {
            Polygon cellGeometry = cellSpace.getGeom();
            closestCellIndex++;
            if(cellGeometry.intersects(bufferedPolygon)){
                resultWithArea.put(closestCellIndex, cellGeometry.intersection(bufferedPolygon).getArea());
            }
        }

        double maxArea = 0.0;
        int selectedIndex = -1;
        for(Integer cellIndex : resultWithArea.keySet()) {
            if(maxArea < resultWithArea.get(cellIndex)) {
                maxArea = resultWithArea.get(cellIndex);
                selectedIndex = cellIndex;
            }
        }

        return selectedIndex;
    }

    /**
     * This function return the trajectory of a person as much as the maximum distance in one second to a given trajectory.
     *
     * @exception Exception If given trajectory length is shorter than given max indoor distance then throw exception
     * @param trajectory Given indoor route
     * @param maxIndoorDistance The maximum distance a person can travel per second
     * @return The movement trajectory of a person as much as the maximum movable distance in one second to a given trajectory
     * */
    public static LineString getIndoorRoute(LineString trajectory, double maxIndoorDistance) throws Exception {
        if(trajectory.getLength() < maxIndoorDistance)
            throw new Exception("Trajectory length is shorter than maxIndoorDistance");

        // Make trajectory to line segment list
        Coordinate[] coordinates = trajectory.getCoordinates();
        ArrayList<LineSegment> lineSegments = new ArrayList<>();
        for(int i = 0; i < coordinates.length - 1; i++) {
            LineSegment lineSegment = new LineSegment();
            lineSegment.setCoordinates(coordinates[i], coordinates[i + 1]);
            lineSegments.add(lineSegment);
        }

        // Find a point related with maximum indoor distance using JTS function(pointAlong(fraction))
        ArrayList<Coordinate> pathCoordinates = new ArrayList<>();
        double remainDistance = maxIndoorDistance;
        for (LineSegment lineSegment : lineSegments) {
            pathCoordinates.add(lineSegment.p0);

            if(remainDistance > lineSegment.getLength()) {
                remainDistance -= lineSegment.getLength();
            }
            else {
                double segmentLengthFraction = remainDistance / lineSegment.getLength();
                pathCoordinates.add(lineSegment.pointAlong(segmentLengthFraction));
                break;
            }
        }

        return createLineString(pathCoordinates);
    }

    /**
     * Make JTS LineString using JTS Coordinate list.
     *
     * @param coordList Coordinate list
     * @return JTS LineString made by coordinate list
     * */
    public static LineString createLineString(ArrayList<Coordinate> coordList) {
        Coordinate[] resultCoordinates = new Coordinate[coordList.size()];
        for(int i = 0; i < resultCoordinates.length; i++) {
            resultCoordinates[i] = coordList.get(i);
        }

        return gf.createLineString(resultCoordinates);
    }

    public static boolean isValidByIndoorDistance(LineString trajectory, double maxIndoorDistance, IndoorFeatures indoorFeatures) {
        boolean isValid = true;
        boolean[][] topoGraph = indoorFeatures.getTopologyGraph();
        ArrayList<CellSpace> cellSpaces = indoorFeatures.getCellSpaces();
        ArrayList<Integer> cellIndexHistory = new ArrayList<>();    // 입력된 궤적의 NSMM 결과
        Coordinate startP = trajectory.getCoordinates()[0];
        cellIndexHistory.add(getCellSpaceIndexWithEpsilon(startP, indoorFeatures.getCellSpaces()));

        for(int i = 0; i < trajectory.getNumPoints() - 1; i++) {
            startP = trajectory.getCoordinateN(i);
            Coordinate endP = trajectory.getCoordinateN(i + 1);

            LineString trajectorySegments = gf.createLineString(new Coordinate[]{startP, endP});
            IndoorRoute indoorRoute = getIndoorRoute(trajectorySegments, cellSpaces);
            trajectorySegments = indoorRoute.route;
            int nextCellIndex = indoorRoute.lastCellIndex;
            int prevCellIndex = cellIndexHistory.get(cellIndexHistory.size() - 1);

            if(trajectorySegments == null){
                isValid = false;
                System.out.println("There is no indoor route");
                break;
            }
            if(trajectorySegments.getLength() > maxIndoorDistance){
                isValid = false;
                System.out.println("Indoor distance is over than MAX : " + trajectorySegments.getLength());
                break;
            }
            if(!topoGraph[prevCellIndex][nextCellIndex]) {
                isValid = false;
                System.out.println("Topology is not connected " + indoorFeatures.getCellSpaceLabel(prevCellIndex) + " to " + indoorFeatures.getCellSpaceLabel(nextCellIndex));
                break;
            }

            cellIndexHistory.add(nextCellIndex);
        }

        return isValid;
    }

    public static void indoorDistance(Coordinate startP, Coordinate endP, double maxIndoorDistance, IndoorFeatures indoorFeatures) {
        boolean[][] topoGraph = indoorFeatures.getTopologyGraph();
        ArrayList<CellSpace> cellSpaces = indoorFeatures.getCellSpaces();

        LineString trajectorySegments = gf.createLineString(new Coordinate[]{startP, endP});
        IndoorRoute indoorRoute = getIndoorRoute(trajectorySegments, cellSpaces);
        trajectorySegments = indoorRoute.route;
        int nextCellIndex = indoorRoute.lastCellIndex;
        int prevCellIndex = getCellSpaceIndexWithEpsilon(startP, indoorFeatures.getCellSpaces());
        Point endPoint = gf.createPoint(endP);

        if(trajectorySegments == null){
            System.out.println("There is no indoor route");
        }
        if(trajectorySegments.getLength() > maxIndoorDistance){
            System.out.println("Indoor distance is over than MAX : " + trajectorySegments.getLength());
        }

        if(trajectorySegments != null
                && trajectorySegments.getLength() > maxIndoorDistance
                && prevCellIndex != nextCellIndex) {
            HashSet<Integer> candidateSet = new HashSet<>();
            for(int j = 0; j < cellSpaces.size(); j++) {
                if(topoGraph[j][nextCellIndex] || topoGraph[prevCellIndex][j]) {
                    int connectedCellCount = 0;
                    for(int k = 0; k < cellSpaces.size(); k++) {
                        if(topoGraph[j][k])
                            connectedCellCount++;
                    }
                    if(connectedCellCount > 5) {
                        candidateSet.add(j);
                        System.out.println("candidate Cell: " + indoorFeatures.getCellSpaceLabel(j));
                    }
                }
            }
            System.out.println("candidate Count: " + candidateSet.size());

            double minDist = maxIndoorDistance;
            Coordinate tmpCoordinate;
            for(Integer cellIndex : candidateSet) {
                Polygon nextCellSpace = cellSpaces.get(cellIndex).getGeom();
                Geometry targetSegments = trajectorySegments.intersection(nextCellSpace);
                if (!targetSegments.isEmpty()) {
                    Coordinate[] coordinates = DistanceOp.nearestPoints(targetSegments, endPoint);
                    tmpCoordinate = coordinates[0];
                } else {
                    Point pts = getNearestPoint(endPoint, nextCellSpace);
                    tmpCoordinate = pts.getCoordinate();
                }

                double tmpDist = tmpCoordinate.distance(endP);
                if (minDist > tmpDist) {
                    minDist = tmpDist;
                    System.out.println("corrected to " + indoorFeatures.getCellSpaceLabel(cellIndex));
                }
            }
        }
        else {
            if(!cellSpaces.get(nextCellIndex).getGeom().covers(endPoint)) {
                System.out.println("It seems to located in the outdoor space");
            }
            else {
                System.out.println("Using original endP");
            }
        }
    }

    /**
     * A function that apply a filter using the maximum distance that a person can move per second for indoor route.
     * This function assumes that the sampling time of each point of a given trajectory is one second.
     * Therefore, If the length of each line segments exceeds the maximum distance, a new end point is generated.
     * It is also the start point of the next line segment.
     *
     * @param trajectory Given Indoor route
     * @param maxIndoorDistance The maximum distance a person can travel per second
     * @param indoorFeatures List of CellSpace that contains all indoor space information in the building
     * @param sameCount This is a variable that determines whether the number of points constituting the newly created path
     *                  should be the same as the number op points constituting the input path. Default value is false.
     * @return Indoor route with maximum indoor distance filter
     * */
    public static LineString applyIndoorDistanceFilter(LineString trajectory, double maxIndoorDistance, IndoorFeatures indoorFeatures, boolean sameCount) {
        final double MAX_ERR = 5 * ChangeCoord.CANVAS_MULTIPLE;     //
        Coordinate correctedCoordinate = null;
        boolean[][] topoGraph = indoorFeatures.getTopologyGraph();
        ArrayList<CellSpace> cellSpaces = indoorFeatures.getCellSpaces();
        ArrayList<Coordinate> pathCoordinates = new ArrayList<>();  // 보정된 궤적의 좌표 리스트
        ArrayList<Integer> cellIndexHistory = new ArrayList<>();    // 입력된 궤적의 NSMM 결과

        int nextCellIndex;
        Coordinate startP = trajectory.getCoordinates()[0];
        pathCoordinates.add(startP);
        cellIndexHistory.add(getCellSpaceIndexWithEpsilon(startP, indoorFeatures.getCellSpaces()));

        for(int i = 0; i < trajectory.getNumPoints() - 1; i++) {
            Coordinate endP = trajectory.getCoordinateN(i + 1);

            // 이전 좌표(endP 직전의 좌표)가 수정되었다면 수정된 좌표를 사용
            if(correctedCoordinate == null) {
                startP = trajectory.getCoordinateN(i);
            }
            else {
                startP = correctedCoordinate;
            }

            LineString trajectorySegments = gf.createLineString(new Coordinate[]{startP, endP});
            IndoorRoute indoorRoute = getIndoorRoute(trajectorySegments, cellSpaces);
            trajectorySegments = indoorRoute.route;
            nextCellIndex = indoorRoute.lastCellIndex;
            int prevCellIndex = cellIndexHistory.get(cellIndexHistory.size() - 1);
            Point endPoint = gf.createPoint(endP);

            if(trajectorySegments != null
                    && trajectorySegments.getLength() > maxIndoorDistance
                    && prevCellIndex != nextCellIndex) {
                // startP와 endP간의 indoor route의 길이가 maxIndoorDistance보다 크면서 startP와 endP가 각자 다른 셀에 속하는 경우

                // 먼저 endP 근처에 MAX_ERR 만큼의 반경 내의 셀들의 인덱스를 후보로 찾는다.
                // 단, 이때 직전의 셀 인덱스 (prevCellIndex)는 반드시 후보에 포함시킨다.
                //Polygon circleBuffer = (Polygon) endPoint.buffer(MAX_ERR);
                HashSet<Integer> candidateSet = new HashSet<>();
                for(int j = 0; j < cellSpaces.size(); j++) {
                    //if(j == nextCellIndex) continue;
                    if(topoGraph[j][nextCellIndex] || topoGraph[prevCellIndex][j]) {
                        int connectedCellCount = 0;
                        for(int k = 0; k < cellSpaces.size(); k++) {
                            if(topoGraph[j][k])
                                connectedCellCount++;
                        }
                        if(connectedCellCount > 5)
                            candidateSet.add(j);
                    }
                    //else if(cellSpaces.get(j).getGeom().intersects(circleBuffer))
                      //  candidateSet.add(j);
                }
                //candidateSet.add(prevCellIndex);


                double minDist = MAX_ERR;
                //final int tmpNextCellIndex = nextCellIndex;
                //int MAX_COUNT = 0;
                Coordinate tmpCoordinate;
                for(Integer cellIndex : candidateSet) {
                    Polygon nextCellSpace = cellSpaces.get(cellIndex).getGeom();
                    Geometry targetSegments = trajectorySegments.intersection(nextCellSpace);
                    if(!targetSegments.isEmpty()) {
                        Coordinate[] coordinates = DistanceOp.nearestPoints(targetSegments, endPoint);
                        tmpCoordinate = coordinates[0];
                    }
                    else {
                        Point pts = getNearestPoint(endPoint, nextCellSpace);
                        tmpCoordinate = pts.getCoordinate();
                    }

                    double tmpDist = tmpCoordinate.distance(endP);
                    if(minDist > tmpDist) {
                        minDist = tmpDist;
                        correctedCoordinate = tmpCoordinate;
                        nextCellIndex = cellIndex;
                    }
                    /*
                    int connectedCellCount = 0;
                    for(int j = 0; j < cellSpaces.size(); j++) {
                        if(topoGraph[cellIndex][j])
                            connectedCellCount++;
                    }
                    if(MAX_COUNT < connectedCellCount) {
                        MAX_COUNT = connectedCellCount;
                        nextCellIndex = cellIndex;
                    }
                     */

                    /*
                    // 각 후보셀과 endP 간의 최단 거리를 구한다.
                    double euclideanDist = cellSpaces.get(cellIndex).getGeom().distance(endPoint);

                    // 만약 후보셀이 endP가 포함된 (혹은 가장 가까운) 셀과 topology적으로 연결되어 있으며 두 셀이 다른 셀인 경우
                    // 최단거리 값을 1/100로 만든다. 즉 가중치를 부여한다.
                    if((topoGraph[cellIndex][tmpNextCellIndex] && cellIndex != tmpNextCellIndex))
                        euclideanDist *= 0.01;

                    // 후보 중 가장 가까운 거리를 가진 cellSpace를 endP가 포함 될 공간으로 선택한다
                    if(minDist > euclideanDist) {
                        minDist = euclideanDist;
                        nextCellIndex = cellIndex;
                    }
                    */
                }

                // 선정된 cellSpace 내에서 endP와 가장 가까운 점을 찾는다
                /*
                Polygon nextCellSpace = cellSpaces.get(nextCellIndex).getGeom();
                Geometry targetSegments = trajectorySegments.intersection(nextCellSpace);
                if(!targetSegments.isEmpty()) {
                    Coordinate[] coordinates = DistanceOp.nearestPoints(targetSegments, endPoint);
                    correctedCoordinate = coordinates[0];
                }
                else {
                    Point pts = getNearestPoint(endPoint, nextCellSpace);
                    correctedCoordinate = pts.getCoordinate();
                }
                 */


                /*
                Point pts = getNearestPoint(endPoint, nextCellSpace);

                // 보정된 점과 startP간의 거리가 설정한 값(MAX_ERR)이내이거나 endP가 실외에 존재하는 경우 보정된 점을 사용
                if(pts.distance(gf.createPoint(startP)) < MAX_ERR || !nextCellSpace.covers(endPoint))
                    correctedCoordinate = pts.getCoordinate();
                else {
                    correctedCoordinate = endPoint.getCoordinate();
                    System.out.println("Used original one");
                }
                */
            }
            else {
                // 맥스 거리 이내의 경우
                if(!cellSpaces.get(nextCellIndex).getGeom().covers(endPoint))
                    // endP가 실내에 존재하지 않는 경우 nextCellIndex에 해당되는 cellSpace에 포함되면서 endP에 가장 가까운 점을 endP로 만들어 준다
                    correctedCoordinate = getNearestPoint(endPoint, cellSpaces.get(nextCellIndex).getGeom()).getCoordinate();
                else
                    correctedCoordinate = endPoint.getCoordinate();
            }
            pathCoordinates.add(correctedCoordinate);
            cellIndexHistory.add(nextCellIndex);
        }

        return createLineString(pathCoordinates);
    }

    /**
     * A function that apply a filter using the maximum distance that a person can move per second for indoor route.
     * This function assumes that the sampling time of each point of a given trajectory is one second.
     * Therefore, If the length of each line segments exceeds the maximum distance, a new end point is generated.
     * It is also the start point of the next line segment.
     *
     * @param trajectory Given Indoor route
     * @param maxIndoorDistance The maximum distance a person can travel per second
     * @param indoorFeatures List of CellSpace that contains all indoor space information in the building
     * @param sameCount This is a variable that determines whether the number of points constituting the newly created path
     *                  should be the same as the number op points constituting the input path. Default value is false.
     * @return Indoor route with maximum indoor distance filter
     * */
    public static LineString applyIndoorDistanceFilter_old(LineString trajectory, double maxIndoorDistance, IndoorFeatures indoorFeatures, boolean sameCount) {
        final int windowSize = 5;
        Coordinate correctedCoordinate = null;
        boolean[][] topoGraph = indoorFeatures.getTopologyGraph();
        ArrayList<CellSpace> cellSpaces = indoorFeatures.getCellSpaces();
        ArrayList<Coordinate> pathCoordinates = new ArrayList<>();
        ArrayList<Integer> cellIndexHistory = new ArrayList<>();

        int lastCellIndex;
        Coordinate startP = trajectory.getCoordinates()[0];
        pathCoordinates.add(startP);
        cellIndexHistory.add(getCellSpaceIndexWithEpsilon(startP, indoorFeatures.getCellSpaces()));
        for(int i = 0; i < trajectory.getNumPoints() - 1; i++) {
            Coordinate endP = trajectory.getCoordinateN(i + 1);

            Boolean usedCorrected = false;
            if(correctedCoordinate == null) {
                startP = trajectory.getCoordinateN(i);
            }
            else {
                startP = correctedCoordinate;
                correctedCoordinate = null;
                usedCorrected = true;
            }

            Coordinate[] trajectoryCoordinates = new Coordinate[]{startP, endP};
            LineString trajectorySegment = gf.createLineString(trajectoryCoordinates);
            IndoorRoute indoorRoute = getIndoorRoute(trajectorySegment, cellSpaces);
            trajectorySegment = indoorRoute.route;
            lastCellIndex = indoorRoute.lastCellIndex;

            int prevCellIndex = cellIndexHistory.get(cellIndexHistory.size() - 1);
            if(trajectorySegment != null
                    && trajectorySegment.getLength() > maxIndoorDistance
                    && prevCellIndex != lastCellIndex) {
                try {
                    /* Calculate route using maximum indoor distance
                    LineString correctedPath = getIndoorRoute(trajectorySegment, maxIndoorDistance);

                    correctedCoordinate = correctedPath.getEndPoint().getCoordinate();
                    if(sameCount) {
                        pathCoordinates.add(correctedCoordinate);
                    }
                    else {
                        for(int j = 1; j < correctedPath.getCoordinates().length; j++){
                            Coordinate coord = correctedPath.getCoordinateN(j);
                            pathCoordinates.add(coord);
                        }
                    }
                    */
                    // Firstly, making a candidate list of cell list (Connected cell from a cell that contained an endP)
                    int tmpIndex;
                    if(usedCorrected) {
                        tmpIndex = lastCellIndex;
                    } else {
                        tmpIndex = prevCellIndex;
                    }
                    HashSet<Integer> candidateIndexSet = new HashSet<>();
                    for(int j = 0; j < topoGraph.length; j++) {
                        if(topoGraph[tmpIndex][j] && tmpIndex != j) {
                            candidateIndexSet.add(j);
                        }
                    }
                    // If candidate size is over than 0,
                    // Check it if there is a candidate index in trajectory history (within a window size)
                    if(candidateIndexSet.size() > 0) {
                        tmpIndex = -1;
                        for(int k = cellIndexHistory.size() - 1;
                            k > cellIndexHistory.size() - (1 + windowSize) && k > 0; k--) {
                            // TODO: If there are multiple results?
                            if(candidateIndexSet.contains(cellIndexHistory.get(k))) {
                                tmpIndex = cellIndexHistory.get(k);
                                break;
                            }
                        }
                        // If there is, we choose it
                        if(tmpIndex != -1) {
                            lastCellIndex = tmpIndex;
                        }
                        // Otherwise, we choose the first candidate
                        else {
                            candidateIndexSet.clear();
                            for(int j = 0; j < topoGraph.length; j++) {
                                if(topoGraph[lastCellIndex][j] && lastCellIndex != j) {
                                    candidateIndexSet.add(j);
                                }
                            }
                            lastCellIndex = candidateIndexSet.iterator().next();
                        }
                        // Lastly, we find the nearest point from selected CellSpace
                        Polygon cellSpace = cellSpaces.get(lastCellIndex).getGeom();
                        Point pts = getNearestPoint(gf.createPoint(endP), cellSpace);
                        correctedCoordinate = pts.getCoordinate();
                        pathCoordinates.add(correctedCoordinate);
                        cellIndexHistory.add(lastCellIndex);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                if(!cellSpaces.get(lastCellIndex).getGeom().covers(gf.createPoint(endP)))
                    endP = getNearestPoint(gf.createPoint(endP), cellSpaces.get(lastCellIndex).getGeom()).getCoordinate();
                pathCoordinates.add(endP);
                cellIndexHistory.add(lastCellIndex);
            }
        }

        return createLineString(pathCoordinates);
    }

    /**
     * A function that apply a filter using the maximum distance that a person can move per second for indoor route.
     * This function assumes that the sampling time of each point of a given trajectory is one second.
     * Therefore, If the length of each line segments exceeds the maximum distance, a new end point is generated.
     * It is also the start point of the next line segment.
     *
     * @param trajectory Given Indoor route
     * @param maxIndoorDistance The maximum distance a person can travel per second
     * @param indoorFeatures List of CellSpace that contains all indoor space information in the building
     * @return Indoor route with maximum indoor distance filter
     * */
    public static LineString applyIndoorDistanceFilter(LineString trajectory, double maxIndoorDistance, IndoorFeatures indoorFeatures) {
       return applyIndoorDistanceFilter(trajectory, maxIndoorDistance, indoorFeatures, true);
    }

    /**
     * This function adds noise to the input trajectory.
     *
     *  @param trajectory Input trajectory
     *  @param noiseRange Range of noise
     * */
    public static LineString generatePathAddedNoise(LineString trajectory, final double noiseRange) {
        LineString resultPath = null;

        // Make trajectory to line segment list
        Coordinate[] coordinates = trajectory.getCoordinates();
        ArrayList<LineSegment> lineSegments = new ArrayList<>();
        for(int i = 0; i < coordinates.length - 1; i++) {
            LineSegment lineSegment = new LineSegment();
            lineSegment.setCoordinates(coordinates[i], coordinates[i + 1]);
            lineSegments.add(lineSegment);
        }

        return resultPath;
    }
}